<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids\Fixtures;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use RicoGrids\Loader\TemplateRegistrationLoader;
use RicoGrids\Models\EntryTemplate;
use RicoGrids\Models\Grid;
use RicoGrids\Models\GridEntry;
use RicoGrids\Models\GridTemplate;
use Shopware\Models\Media\Media;
use Shopware\Models\Shop\Shop;
use sRewriteTable;
use Symfony\Component\Finder\Finder;

/**
 * Class GridFixture
 */
class GridFixture
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var sRewriteTable
     */
    private $rewriteTable;

    /**
     * @var string
     */
    private $pluginPath = '';

    /**
     * @var string
     */
    private $gridPath = '/Resources/views/frontend/rico_grid/grid/';

    /**
     * @var string
     */
    private $entryPath = '/Resources/views/frontend/rico_grid/entry/';

    /**
     * @var
     */
    private $gridTemplates;
    /**
     * @var
     */
    private $entryTemplates;

    public function __construct(EntityManager $entityManager, sRewriteTable $rewriteTable, string $pluginPath)
    {
        $this->entityManager = $entityManager;
        $this->rewriteTable = $rewriteTable;
        $this->pluginPath = $pluginPath;
    }

    /**
     * @param int $gridAmount
     * @param int $gridEntryAmount
     *
     * @return bool
     */
    public function load($gridAmount = 3, $gridEntryAmount = 5)
    {
        $this->registerTemplates();

        /** @var Media $media */
        /** @var Shop $shop */
        $media = $this->entityManager->getRepository(Media::class)->findOneBy(['type' => 'IMAGE']);
        $shops = new ArrayCollection();
        $shop = $this->entityManager->getRepository(Shop::class)->findOneBy(['default' => true]);
        $shops->add($shop);

        if (!$media || empty($media) || !$shop || empty($shop)) {
            return false;
        }

        for ($i = 0; $i < $gridAmount; ++$i) {
            $grid = new Grid();
            $grid->setShop($shops);
            $grid->setName('grid_nr' . $i);
            $grid->setDescription('grid description ' . $i);
            switch ($i) {
                case 0:
                    $gridType = 'lightbox';
                    break;
                case 1:
                    $gridType = 'external';
                    break;
                default:
                    $gridType = 'internal';
                    $grid->setFrontendLayout($this->entryTemplates[0]);
                    break;
            }
            $grid->setGridLayout($this->gridTemplates[0]);
            $grid->setType($gridType);
            switch ($i) {
                case 0:
                    $grid->setOrderType(1);
                    break;
                case 1:
                    $grid->setOrderType(2);
                    break;
                default:
                    $grid->setOrderType(3);
                    break;
            }
            switch ($i) {
                case 0:
                    $grid->setActive(false);
                    break;
                default:
                    $grid->setActive(true);
                    break;
            }
            $gridEntries = new ArrayCollection();
            for ($x = 0; $x < $gridEntryAmount; ++$x) {
                $gridEntry = new GridEntry();
                $gridEntry->setName('grid_nr' . $i . '-entry_' . $x);
                $gridEntry->setImage($media);
                switch ($i) {
                    case 0:
                        $gridEntry->setActive(false);
                        break;
                    default:
                        $gridEntry->setActive(true);
                        break;
                }
                switch ($gridType) {
                    case 'internal':
                        $text = [];
                        for ($y = 0; $y < 3; ++$y) {
                            array_push($text, ['text' => $x . '_abc_' . $y]);
                        }
                        $gridEntry->setText($text);
                        $gridEntry->setSubHeading('grid description ' . $i . ' entry ' . $x);
                        break;
                    case 'lightbox':
                        $gridEntry->setSubHeading('grid description ' . $i . ' entry ' . $x);
                        break;
                    case 'external':
                        $gridEntry->setLink('pluginlabor.com');
                        break;
                    default:
                        break 2;
                        break;
                }
                $gridEntry->setBigEntry(false);
                $gridEntry->setPosition($x);
                if (0 === $x % 8) {
                    $gridEntry->setBigEntry(true);
                }
                $gridEntry->setGrid($grid);
                $gridEntries->add($gridEntry);
            }
            $grid->setGridEntries($gridEntries);

            $this->entityManager->persist($grid);
        }
        try {
            $this->entityManager->flush();

            return true;
        } catch (OptimisticLockException $e) {
            Shopware()->Container()->get('pluginlogger')->error(json_encode($e));

            return false;
        }
    }

    private function registerTemplates()
    {
        $templateRegisterService = new TemplateRegistrationLoader(
            $this->entityManager,
            $this->pluginPath
        );
        $templateRegisterService->registerGridTemplates(new Finder(), $this->gridPath);
        $templateRegisterService->registerEntryLayoutTemplates(new Finder(), $this->entryPath);
        $this->gridTemplates = $this->entityManager->getRepository(GridTemplate::class)->findAll();
        $this->entryTemplates = $this->entityManager->getRepository(EntryTemplate::class)->findAll();
    }
}
