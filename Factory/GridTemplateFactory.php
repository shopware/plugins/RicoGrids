<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids\Factory;

use Doctrine\ORM\OptimisticLockException;
use RicoGrids\Models\GridTemplate;
use Shopware\Components\Model\ModelManager;

/**
 * Class TemplateRegistrationService.
 */
class GridTemplateFactory
{
    /**
     * @param string $template
     *
     * @return GridTemplate|bool
     */
    public static function build(ModelManager $manager, $template)
    {
        $gridLayout = $manager->getRepository(GridTemplate::class)->findOneBy([
            'name' => $template,
        ]);
        if ($gridLayout) {
            return $gridLayout;
        }

        $newTemplate = new GridTemplate();
        $newTemplate->setName($template);
        $manager->persist($newTemplate);
        try {
            $manager->flush();
        } catch (OptimisticLockException $e) {
            Shopware()->Container()->get('pluginlogger')->error(json_encode($e));

            return false;
        }

        $gridLayout = $manager->getRepository(GridTemplate::class)->findOneBy([
            'name' => $template,
        ]);
        if ($gridLayout) {
            return $gridLayout;
        }

        return false;
    }
}
