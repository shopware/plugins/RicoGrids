<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids\Subscriber;

use Enlight\Event\SubscriberInterface;
use Enlight_Controller_ActionEventArgs;
use Enlight_Event_EventArgs;
use Enlight_Template_Manager;
use RicoGrids\Components\Translation\TranslationOverlayComponent;
use RicoGrids\Models\Grid;
use Shopware\Components\Model\ModelManager;

/**
 * Class RewriteTable
 */
class RewriteTableSubscriber implements SubscriberInterface
{
    /**
     * @var TranslationOverlayComponent
     */
    protected $translationOverlayComponent;

    /**
     * @var string
     */
    private $pluginDirectory;

    /**
     * @var Enlight_Template_Manager
     */
    private $templateManager;

    /**
     * @var ModelManager
     */
    private $modelManager;

    public function __construct(
        $pluginDirectory,
        Enlight_Template_Manager $templateManager,
        ModelManager $modelManager,
        TranslationOverlayComponent $translationOverlayComponent
    ) {
        $this->pluginDirectory = $pluginDirectory;
        $this->templateManager = $templateManager;
        $this->modelManager = $modelManager;
        $this->translationOverlayComponent = $translationOverlayComponent;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'Enlight_Controller_Action_PostDispatch_Backend' => 'registerTemplateDir',
            'Enlight_Controller_Action_PostDispatch_Backend_Performance' => 'loadPerformanceExtension',
            'Shopware_Controllers_Seo_filterCounts' => 'addGridCount',
        ];
    }

    public function registerTemplateDir(Enlight_Controller_ActionEventArgs $args): void
    {
        $args->getSubject()->View()->addTemplateDir($this->pluginDirectory . '/Resources/views/');
    }

    public function loadPerformanceExtension(Enlight_Controller_ActionEventArgs $args): void
    {
        $subject = $args->getSubject();
        $request = $subject->Request();

        if ('load' !== $request->getActionName()) {
            return;
        }

        $subject->View()->addTemplateDir($this->pluginDirectory . '/Resources/views/');
        $subject->View()->extendsTemplate('backend/performance/view/main/grid.js');
    }

    public function addGridCount(Enlight_Event_EventArgs $eventArgs): array
    {
        $counts = $eventArgs->getReturn();
        $shopId = (int) $eventArgs->get('shopId');

        // Fetch / localize grids.
        $gridRepository = $this->modelManager->getRepository(Grid::class);
        $grids = $gridRepository->findAll();
        if ($shopId > 1) {
            $grids = $this->translationOverlayComponent->translate($shopId, $grids, 'grid');
        }

        // Get / localize grid entries.
        $gridEntriesCollection = [];
        /** @var Grid $grid */
        foreach ($grids as $grid) {
            $gridEntries = $grid->getGridEntries();
            if ($shopId > 1) {
                $gridEntries = $this->translationOverlayComponent->translate($shopId, $grids, 'grid_entry');
            }
            array_walk($gridEntries, function ($gridEntry) use ($gridEntriesCollection) {
                $gridEntriesCollection[] = $gridEntry;
            });
        }
        $counts['grid'] = count($grids) + count($gridEntriesCollection);

        return $counts;
    }
}
