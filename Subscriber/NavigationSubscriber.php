<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids\Subscriber;

use Enlight\Event\SubscriberInterface;
use RicoGrids\Loader\RicoMenuLoader;
use Shopware\Components\DependencyInjection\Container;
use Shopware\Components\Model\ModelManager;

/**
 * Class NavigationSubscriber
 */
class NavigationSubscriber implements SubscriberInterface
{
    /** @var Container */
    private $container;

    /** @var ModelManager */
    private $modelManager;

    /**
     * NavigationSubscriber constructor.
     */
    public function __construct(Container $container, ModelManager $modelManager)
    {
        $this->container = $container;
        $this->modelManager = $modelManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatch_Backend_RicoCustomNavigation' => 'registerMenu',
        ];
    }

    /**
     * Registers the the grid-type for the custom navigation plugin
     */
    public function registerMenu()
    {
        $menuRegistration = new RicoMenuLoader(
            $this->modelManager,
            $this->container->get('rico_custom_navigation.type_service'));
        $menuRegistration->registerEntry();
    }
}
