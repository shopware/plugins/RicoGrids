<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids\Subscriber;

use Enlight\Event\SubscriberInterface;
use Enlight_Event_EventArgs;
use Enlight_Template_Manager;
use RicoGrids\Loader\TemplateRegistrationLoader;
use Symfony\Component\Finder\Finder;

/**
 * Class GridTemplatesSubscriber
 */
class GridTemplatesSubscriber implements SubscriberInterface
{
    /**
     * @var string
     */
    private $pluginDirectory;

    /**
     * @var Enlight_Template_Manager
     */
    private $templateManager;

    /**
     * defines the template Path
     *
     * @var string
     */
    private $gridPath = '/Resources/views/frontend/rico_grid/grid/';
    private $entryPath = '/Resources/views/frontend/rico_grid/entry/';

    /**
     * @param $pluginDirectory
     */
    public function __construct($pluginDirectory, Enlight_Template_Manager $templateManager)
    {
        $this->pluginDirectory = $pluginDirectory;
        $this->templateManager = $templateManager;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            'Enlight_Controller_Action_PostDispatch_Backend_RicoGrids' => 'registerGridTemplates',
        ];
    }

    /**
     * @return bool
     */
    public function registerGridTemplates(Enlight_Event_EventArgs $eventArgs)
    {
        if ($eventArgs->get('templatesRegistered')) {
            return true;
        }
        $eventArgs->set('templatesRegistered', true);
        $templateRegisterService = new TemplateRegistrationLoader(
            Shopware()->Container()->get('models'),
            $this->pluginDirectory
        );
        $templateRegisterService->registerGridTemplates(new Finder(), $this->gridPath);
        $templateRegisterService->registerEntryLayoutTemplates(new Finder(), $this->entryPath);

        return true;
    }
}
