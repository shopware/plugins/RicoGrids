//  {namespace name=backend/rico_grids/view/grid/window}
Ext.define('Shopware.apps.RicoGrids.view.grid.Window', {
    extend: 'Enlight.app.Window',

    border: false,
    autoShow: true,
    height: '90%',
    width: '80%',
    modal: false,
    layout: {
        type: 'fit'
    },

    title: '{s name="windowTitle"}{/s}',

    alias: 'widget.rico-grid-grid-window',
    eventAlias: 'rico-grid-grid-window',

    stateId: 'rico-grid-grid-window',
    tabPanel: null,

    initComponent: function() {
        var me = this;

        me.registerEvents();

        me.tabPanel = Ext.create('Ext.tab.Panel', {
            items: me.getTabs(),
            split: false,
            flex: 1,
            autoScroll: true,
            height: '100%',
            listeners: {
                'tabchange': function (tabPanel, newCard, oldCard) {
                    me.fireEvent('tabChange', tabPanel, newCard, oldCard);
                }
            }
        });

        me.formPanel = Ext.create('Ext.form.Panel', {
            items: [me.tabPanel],
            region: 'center',
            border: false,
            name: 'mainFormPanel',
            autoScroll: true,
            height: '100%',
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            dockedItems: me.getDockedItems()
        });

        me.items = [
            me.formPanel
        ];

        me.fireEvent('windowLoaded', me);

        me.callParent(arguments);
    },

    getTabs: function() {
        var me = this;
        return [
            {
                xtype: 'rico-grid-grid-tab-configuration',
                record: me.record,
                store: me.gridStore,
                entryTemplateStore: me.entryTemplateStore,
                gridTemplateStore: me.gridTemplateStore
            },
            {
                xtype: 'rico-grid-grid-tab-entry-list',
                record: me.record,
                store: me.entryListStore
            }
        ];
    },

    getDockedItems: function() {
        var me = this,
            menu = ['->'];
        menu.push({
            text: '{s name=main/window/button/save}Save{/s}',
            action: 'saveDetail',
            disabled: false,
            cls: 'primary',
            handler: function() {
                me.fireEvent('saveDetail', me);
            }
        });

        return [{
            xtype: 'toolbar',
            cls: 'shopware-toolbar',
            dock: 'bottom',
            ui: 'shopware-ui',
            items: menu
        }];
    },

    registerEvents: function() {
        var me = this;
        me.addEvents(
            'windowLoaded'
        );
    }
});
