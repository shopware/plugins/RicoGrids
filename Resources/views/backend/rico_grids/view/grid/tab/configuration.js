//  {namespace name=backend/rico_grids/view/grid/tab/configuration}
Ext.define('Shopware.apps.RicoGrids.view.grid.tab.Configuration', {
    extend: 'Ext.form.Panel',
    /**
     * Register the alias for this class.
     * @string
     */
    alias: 'widget.rico-grid-grid-tab-configuration',

    cls: 'shopware-form',
    /**
     * Title of this tab
     * @string
     */
    title: '{s name=configuration_title}Configuration{/s}',
    /**
     * Specifies the border for this component. The border can be a single numeric
     * value to apply to all sides or it can be a CSS style specification for each
     * style, for example: '10 5 3 10'.
     *
     * Default: 0
     * @integer
     */
    border: 0,
    /**
     * Display the the contents of this tab immediately
     * @boolean
     */
    autoShow: true,
    autoScroll: true,
    layout: 'fit',
    bodyPadding: 10,
    height: '100%',

    /**
     * Translations
     * @object
     */
    snippets: {
        form: {
            base: {
                title: 'Base title',
                linkName: 'Base name',
                shopLabel: 'Shop label',
                frontendAssignLabel: 'Frontend assign label',
                active: 'Active'
            },
            entry: {
                title: 'Entry title',
                selectType: 'Type label',
                linkLabel: 'Link label',
                categoryLabel: 'Category label',
                articleLabel: 'Article label',
                landingPageLabel: 'LandingPage label',
                siteLabel: 'Site label',
                blogLabel: 'Blog label'
            }
        }
    },
    /**
     * Single Form elements to access them from the controller
     */
    /**
     * Form part containing the form with the default category settings
     * @object [Ext.form.FieldSet]
     */
    baseSettings: null,

    /**
     * default field attributes
     */
    defaults: {
        anchor: '100%',
        labelWidth: 155
    },

    plugins: [{
        pluginId: 'translation',
        ptype: 'translation',
        translationType: 'grid',
        translationKey: 'grid'
    }],

    initComponent: function () {
        var me = this;

        me.createCustomStores();
        me.items = me.getItems();
        me.plugins = [{
            pluginId: 'translation',
            ptype: 'translation',
            translationType: 'grid',
            translationKey: null
        }];

        me.callParent(arguments);
    },

    getItems: function() {
        var me = this;

        me.baseForm = me.getBaseForm();
        var attributeForm = Ext.create('Shopware.attribute.Form', {
            table: 'rico_grid_attributes',
            allowTranslation: true,
            name: 'gridAttributeForm',
            id: 'gridAttributeForm'
        });
        return [ me.baseForm, attributeForm ];
    },

    getBaseForm: function() {
        var me = this;

        return Ext.create('Shopware.apps.Base.view.element.Fieldset', {
            height: '100%',
            defaults: me.defaults,
            disabled: false,
            autoScroll: true,
            items: me.getBaseSettingItems(),
            translatable: true
        });
    },

    getBaseSettingItems: function() {
        var me = this;
        return [
            {
                xtype: 'textfield',
                fieldLabel: 'gridName',
                dataIndex: 'name',
                name: 'gridName',
                helpText: 'The name of the grid',
                translatable: true,
                translationName: 'name',
                allowBlank: false
            },
            {
                xtype: 'tinymce',
                fieldLabel: 'gridDescription',
                dataIndex: 'description',
                name: 'gridDescription',
                translatable: true,
                translationName: 'description',
                allowBlank: false
            },
            {
                xtype: 'combo',
                fieldLabel: 'gridType',
                name: 'gridType',
                store: me.typeStore,
                dataIndex: 'type',
                valueField: 'type',
                displayField: 'type',
                listeners: {
                    select: function(combo) {
                        me.fireEvent('gridTypeSelected', me, combo);
                    }
                }
            },
            {
                xtype: 'combo',
                fieldLabel: 'gridSubType',
                name: 'gridSubType',
                hidden: true,
                store: me.subTypeStore,
                dataIndex: 'type',
                valueField: 'type',
                displayField: 'type',
                listeners: {
                    select: function(combo) {
                        me.fireEvent('gridSubTypeSelected', me, combo);
                    }
                }
            },
            {
                xtype: 'combo',
                fieldLabel: 'gridShop',
                name: 'gridShop',
                store: 'Shopware.store.Shop',
                multiSelect: true,
                valueField: 'id',
                displayField: 'name'
            },
            {
                xtype: 'combo',
                fieldLabel: 'frontendLayout',
                name: 'frontendLayout',
                hidden: true,
                store: me.entryTemplateStore,
                valueField: 'id',
                displayField: 'name'
            },
            {
                xtype: 'combo',
                fieldLabel: 'gridLayout',
                name: 'gridLayout',
                store: me.gridTemplateStore,
                valueField: 'id',
                displayField: 'name'
            },
            {
                xtype: 'combo',
                fieldLabel: 'orderField',
                name: 'orderField',
                store: me.orderStore,
                valueField: 'value',
                displayField: 'name'
            },
            {
                xtype: 'checkboxgroup',
                columns: 2,
                defaultType: 'checkboxfield',
                margin: '3 0 0 0',
                defaults: Ext.applyIf({
                    inputValue: true,
                    uncheckedValue: false
                }, me.defaults),
                items: [
                    {
                        boxLabel: 'gridActive',
                        name: 'gridActive',
                        dataIndex: 'active'
                    }
                ] }
        ];
    },

    createCustomStores: function() {
        var me = this;

        me.typeStore = Ext.create('Ext.data.Store', {
            fields: ['type'],
            data: [
                { 'type': 'external' },
                { 'type': 'internal' },
                { 'type': 'lightbox' },
                { 'type': 'placeholder' },
                { 'type': 'accordion' }
            ]
        });
        me.subTypeStore = Ext.create('Ext.data.Store', {
            fields: ['type'],
            data: [
                { 'type': 'external' },
                { 'type': 'internal' },
                { 'type': 'lightbox' },
                { 'type': 'placeholder' }
            ]
        });
        me.orderStore = Ext.create('Ext.data.Store', {
            fields: ['name', 'value'],
            data: [
                { 'name': 'Position', 'value': 1 },
                { 'name': 'Name ASC', 'value': 2 },
                { 'name': 'NAME DESC', 'value': 3 }
            ]
        });
    },

    registerEvents: function() {
        var me = this;

        me.addEvents(
            'gridTypeSelected'
        );
    }
});
