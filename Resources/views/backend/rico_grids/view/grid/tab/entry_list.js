//  {namespace name=backend/rico_grids/view/grid/tab/entry_list}
Ext.define('Shopware.apps.RicoGrids.view.grid.tab.EntryList', {
    extend: 'Ext.panel.Panel',
    border: false,
    alias: 'widget.rico-grid-grid-tab-entry-list',
    region: 'center',
    selectedPreviewSize: 16,
    autoScroll: true,
    store: 'EntryList',
    ui: 'shopware-ui',
    selType: 'cellmodel',
    title: 'Grid entry list',
    layout: {
        type: 'hbox',
        align: 'stretch'
    },
    height: '100%',
    position: {
        hidden: true,
        disabled: true
    },

    initComponent: function () {
        var me = this,
            form = {
                xtype: 'form',
                title: 'Details',
                bodyPadding: 5,
                items: me.getSideBar(),
                height: '100%',
                flex: 3,
                autoScroll: true,
                disabled: true,
                dockedItems: [{
                    xtype: 'toolbar',
                    cls: 'shopware-toolbar',
                    dock: 'bottom',
                    ui: 'shopware-ui',
                    items: [
                        '->',
                        {
                            text: '{s name=main/window/button/save}Save{/s}',
                            action: 'saveSidebar',
                            disabled: false,
                            cls: 'primary',
                            align: 'right'
                        }]
                }]
            };

        if (form.plugins === undefined) {
            form.plugins = [];
        }

        form.plugins.push({
            pluginId: 'translation',
            ptype: 'translation',
            translationType: 'grid_entry',
            translationKey: null
        });

        me.items = [
            {
                xtype: 'grid',
                columns: me.getColumns(),
                selModel: me.getGridSelModel(),
                dockedItems: [ me.getToolbar(), me.getPagingBar() ],
                plugins: [
                    Ext.create('Ext.grid.plugin.RowEditing', {
                        clicksToEdit: 2,
                        autoCancel: true,
                        listeners: {
                            scope: me,
                            edit: function (editor, context) {
                                me.fireEvent('updateList', editor, context);
                            }
                        }
                    })
                ],
                store: me.store,
                flex: 3,
                height: '100%',
                autoScroll: true
            }, {
                xtype: 'splitter'
            }, form
        ];

        me.callParent(arguments);
    },

    registerEvents: function () {
        this.addEvents(
            'deleteEntryListItem',
            'reload',
            'entryListSelectionChange'
        );

        return true;
    },

    getColumns: function () {
        var me = this;

        if (me.record) {
            if (me.record.get('orderType') != 1) {
                me.position.hidden = true;
                me.position.disabled = true;
            } else {
                me.position.hidden = false;
                me.position.disabled = false;
            }
        }

        return [
            {
                header: '{s name=list/column/nameTitle}Name{/s}',
                dataIndex: 'name',
                renderer: me.titleRenderer,
                flex: 6,
                editor: {
                    xtype: 'textfield',
                    inputValue: true,
                    uncheckedValue: false
                }
            },
            {
                header: '{s name=list/column/positionTitle}Position{/s}',
                dataIndex: 'position',
                flex: 1,
                hidden: me.position.hidden,
                disabled: me.position.disabled,
                draggable: false,
                sortable: false,
                groupable: false,
                editor: {
                    xtype: 'numberfield',
                    inputValue: true,
                    uncheckedValue: false
                }
            },
            {
                header: '{s name=list/column/activeTitle}Active{/s}',
                dataIndex: 'active',
                align: 'center',
                renderer: me.activeColumnRenderer,
                flex: 0.5,
                editor: {
                    xtype: 'checkbox',
                    inputValue: true,
                    uncheckedValue: false
                }
            },
            {
                xtype: 'actioncolumn',
                flex: 0.5,
                align: 'center',
                items: me.getActionColumnItems()
            }
        ];
    },

    getActionColumnItems: function () {
        var me = this,
            actionColumnData = [];

        //
        // actionColumnData.push({
        //   iconCls:'sprite-pencil',
        //   cls:'editBtn',
        //   tooltip:'{s name=list/action_column/edit}Edit this grid item{/s}',
        //   handler:function (view, rowIndex, colIndex, item) {
        //     me.fireEvent('editEntryListItem', view, rowIndex, colIndex, item);
        //   }
        // });

        actionColumnData.push({
            iconCls: 'sprite-minus-circle-frame',
            action: 'delete',
            cls: 'delete',
            tooltip: '{s name=list/action_column/delete}Delete this grid item{/s}',
            handler: function (view, rowIndex, colIndex, item) {
                me.fireEvent('deleteEntryListItem', view, rowIndex, colIndex, item);
            }
        });

        return actionColumnData;
    },

    getToolbar: function () {
        return Ext.create('Ext.toolbar.Toolbar',
            {
                dock: 'top',
                ui: 'shopware-ui',
                items: [
                    /* {if {acl_is_allowed privilege=create}} */
                    {
                        iconCls: 'sprite-plus-circle',
                        text: '{s name=list/tab/button/add}Add grid entry{/s}',
                        action: 'addGridEntry'
                    },
                    /* {/if} */
                    /* {if {acl_is_allowed privilege=delete}} */
                    {

                        iconCls: 'sprite-minus-circle-frame',
                        text: '{s name=list/tab/button/delete}Delete selected grid entries{/s}',
                        disabled: true,
                        action: 'deleteGridEntries'
                    },
                    /* {/if} */
                    '->',
                    {
                        xtype: 'textfield',
                        name: 'searchfield',
                        action: 'searchEntries',
                        width: 170,
                        cls: 'searchfield',
                        enableKeyEvents: true,
                        checkChangeBuffer: 500,
                        emptyText: '{s name=list/field/search}Search...{/s}'
                    },
                    { xtype: 'tbspacer', width: 6 }
                ]
            });
    },

    getPagingBar: function () {
        var me = this;
        return Ext.create('Ext.toolbar.Paging', {
            store: me.store,
            dock: 'bottom',
            displayInfo: true
        });
    },

    getGridSelModel: function () {
        var me = this;
        return Ext.create('Ext.selection.CheckboxModel', {
            listeners: {
                // Unlocks the delete button if the user has checked at least one checkbox
                selectionchange: function (sm, selections) {
                    me.fireEvent('entryListSelectionChange', sm, selections);
                    var owner = this.view.ownerCt,
                        btn = owner.down('button[action=deleteGridEntries]');
                    btn.setDisabled(!selections.length);
                }
            }
        });
    },

    getSideBar: function() {
        var me = this;

        if (me.record) {
            if (me.record.get('orderType') != 1) {
                me.position.hidden = true;
                me.position.disabled = true;
            } else {
                me.position.hidden = false;
                me.position.disabled = false;
            }
        }

        me.nameField = {
            xtype: 'textfield',
            fieldLabel: 'nameField',
            dataIndex: 'name',
            name: 'nameField',
            translatable: true,
            translationName: 'name',
            allowBlank: false
        };
        me.positionField = {
            xtype: 'numberfield',
            fieldLabel: 'Position',
            dataIndex: 'position',
            name: 'positionField',
            translatable: false,
            allowBlank: false,
            hidden: me.position.hidden,
            disabled: me.position.disabled
        };
        me.detailNameField = {
            xtype: 'textfield',
            fieldLabel: 'detailNameField',
            dataIndex: 'detailName',
            name: 'detailNameField',
            anchor: '80%',
            hidden: true,
            translatable: true,
            translationName: 'detailName'
        };
        me.subHeadingField = {
            xtype: 'textfield',
            fieldLabel: 'subHeadingField',
            name: 'subHeadingField',
            dataIndex: 'subHeading',
            hidden: true,
            anchor: '80%',
            translatable: true,
            translationName: 'subHeading'
        };
        me.accordionField = {
            xtype: 'textfield',
            fieldLabel: 'accordionField',
            name: 'accordionField',
            hidden: true,
            anchor: '80%'
        };
        me.linkField = {
            xtype: 'textfield',
            fieldLabel: 'linkField',
            name: 'linkField',
            hidden: true
        };
        me.bigField = {
            xtype: 'checkboxgroup',
            columns: 2,
            defaultType: 'checkboxfield',
            margin: '3 0 0 0',
            defaults: Ext.applyIf({
                inputValue: true,
                uncheckedValue: false
            }, me.defaults),
            items: [
                {
                    boxLabel: 'bigField',
                    name: 'bigField',
                    dataIndex: 'bigEntry'
                }
            ],
            translatable: false
        };
        me.imageField = {
            xtype: 'shopware-media-field',
            fieldLabel: 'mediaField',
            name: 'mediaField',
            readOnly: false,
            valueField: 'id',
            supportText: 'supportText',
            multiSelect: false,
            anchor: '100%',
            removeBackground: true,
            validTypes: me.getAllowedExtensions(),
            validTypeErrorFunction: me.getExtensionErrorCallback(),
            translatable: false
        };
        me.detailImageField = {
            xtype: 'shopware-media-field',
            fieldLabel: 'detailMediaField',
            name: 'detailMediaField',
            readOnly: false,
            valueField: 'id',
            supportText: 'supportText',
            multiSelect: false,
            hidden: true,
            anchor: '100%',
            removeBackground: true,
            validTypes: me.getAllowedExtensions(),
            validTypeErrorFunction: me.getExtensionErrorCallback(),
            translatable: false
        };
        me.descriptionTabPanel = Ext.create('Ext.tab.Panel', {
            items: [],
            dockedItems: {
                xtype: 'toolbar',
                dock: 'top',
                items: [
                    {
                        xtype: 'button',
                        iconCls: 'sprite-plus-circle',
                        text: '{s name=entry/sidebar/desc/tab/add}Add new text{/s}',
                        action: 'addEntryText'
                    },
                    {
                        xtype: 'button',
                        iconCls: 'sprite-minus-circle',
                        text: '{s name=entry/sidebar/desc/tab/remove}remove this tab{/s}',
                        action: 'deleteTab'
                    }
                ]
            },
            split: false,
            hidden: true,
            id: 'descTabPanel',
            flex: 1,
            activeTab: 0
        });
        me.attributes = Ext.create('Shopware.attribute.Form', {
            table: 'rico_grid_entry_attributes',
            allowTranslation: true,
            name: 'gridEntryAttributeForm',
            id: 'gridEntryAttributeForm'
        });

        return [
            me.nameField,
            me.positionField,
            me.detailNameField,
            me.accordionField,
            me.linkField,
            me.subHeadingField,
            me.bigField,
            me.imageField,
            me.detailImageField,
            me.descriptionTabPanel,
            me.attributes
        ];
    },

    // region renderer
    titleRenderer: function (value) {
        return Ext.String.format('{literal}<strong style="font-weight: 700;">{0}</strong>{/literal}', value);
    },

    imageRenderer: function (value, tdStyle, record) {
        // fallback for new entries
        if (record.get('image') === null || !record.get('image').type) {
            return '<div class="sprite-blue-document-text" style="height:16px; width:16px;display:inline-block;"></div>';
        }
        var me = this,
            type = record.get('image').type.toLowerCase(),
            result,
            size = me.selectedPreviewSize > 0 ? me.selectedPreviewSize : 16;

        switch (type) {
            case 'video':
                result = '<div class="sprite-blue-document-film" style="height:16px; width:16px;display:inline-block;"></div>';
                break;
            case 'music':
                result = '<div class="sprite-blue-document-music" style="height:16px; width:16px;display:inline-block;"></div>';
                break;
            case 'archive':
                result = '<div class="sprite-blue-document-zipper" style="height:16px; width:16px;display:inline-block;"></div>';
                break;
            case 'pdf':
                result = '<div class="sprite-blue-document-pdf-text" style="height:16px; width:16px;display:inline-block;"></div>';
                break;
            case 'vector':
                result = '<div class="sprite-blue-document-illustrator" style="height:16px; width:16px;display:inline-block;"></div>';
                break;
            case 'image':
                if (Ext.Array.contains(['tif', 'tiff'], record.get('image').extension)) {
                    result = '<div class="sprite-blue-document-image" style="height:16px; width:16px;display:inline-block;"></div>';
                } else {
                    result = Ext.String.format('<div class="small-preview-image"><img src="{media path=[0]}[0]" style="max-width:[1]px;max-height:[1]px;" alt="[2]" /></div>', value.path, size, record.get('image').name);
                }
                break;
            default:
                result = '<div class="sprite-blue-document-text" style="height:16px; width:16px;display:inline-block;"></div>';
                break;
        }

        return result;
    },

    activeColumnRenderer: function(value) {
        if (value) {
            return '<div class="sprite-tick" style="width: 25px; height: 25px;">&nbsp;</div>';
        } else {
            return '<div class="sprite-cross" style="width: 25px; height: 25px;">&nbsp;</div>';
        }
    },
    // endregion

    // region image extension
    getAllowedExtensions: function() {
        return ['gif', 'png', 'jpeg', 'jpg', 'svg'];
    },
    getExtensionErrorCallback: function() {
        return 'onExtensionError';
    },
    onExtensionError: function() {
        var me = this;
        Shopware.Notification.createGrowlMessage(
            me.snippets.errorMessageWrongFileTypeTitle,
            me.snippets.errorMessageWrongFileType, me.snippets.growlMessage);
    }
    // endregion
});
