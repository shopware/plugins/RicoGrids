//  {namespace name=backend/rico_grids/view/main/list}
Ext.define('Shopware.apps.RicoGrids.view.main.List', {
    extend: 'Ext.grid.Panel',
    border: false,
    alias: 'widget.rico-grid-main-list',
    region: 'center',
    autoScroll: true,
    store: 'List',
    ui: 'shopware-ui',
    selType: 'cellmodel',

    initComponent: function () {
        var me = this;
        me.store = me.listStore;
        me.registerEvents();

        me.selModel = me.getGridSelModel();

        me.columns = me.getColumns();
        me.dockedItems = [ me.getToolbar(), me.getPagingBar() ];

        me.callParent(arguments);
    },

    registerEvents: function () {
        this.addEvents(
            'deleteGrid',
            'editGrid',
            'searchGridItem'
        );
    },

    getColumns: function () {
        var me = this;

        return [
            {
                header: '{s name=list/column/title}Name{/s}',
                dataIndex: 'name',
                renderer: me.titleRenderer,
                flex: 6
            },
            {
                header: '{s name=list/column/type}Type{/s}',
                dataIndex: 'type',
                flex: 2
            },
            {
                header: '{s name=list/column/active}Active{/s}',
                dataIndex: 'active',
                renderer: me.activeColumnRenderer,
                flex: 1
            },
            {
                xtype: 'actioncolumn',
                width: 90,
                items: me.getActionColumnItems()
            }
        ];
    },

    getActionColumnItems: function () {
        var me = this,
            actionColumnData = [];

        actionColumnData.push({
            iconCls: 'sprite-pencil',
            cls: 'editBtn',
            tooltip: '{s name=list/action_column/edit}Edit this grid item{/s}',
            handler: function (view, rowIndex, colIndex, item) {
                me.fireEvent('editGrid', view, rowIndex, colIndex, item);
            }
        });
        actionColumnData.push({
            iconCls: 'sprite-minus-circle-frame',
            action: 'delete',
            cls: 'delete',
            tooltip: '{s name=list/action_column/delete}Delete this grid item{/s}',
            handler: function (view, rowIndex, colIndex, item) {
                me.fireEvent('deleteGrid', view, rowIndex, colIndex, item);
            }
        });

        return actionColumnData;
    },

    getToolbar: function () {
        return Ext.create('Ext.toolbar.Toolbar',
            {
                dock: 'top',
                ui: 'shopware-ui',
                items: [
                    /* {if {acl_is_allowed privilege=create}} */
                    {
                        iconCls: 'sprite-plus-circle',
                        text: '{s name=list/button/add}Add grid item{/s}',
                        action: 'addGrid'
                    },
                    /* {/if} */
                    /* {if {acl_is_allowed privilege=delete}} */
                    {

                        iconCls: 'sprite-minus-circle-frame',
                        text: '{s name=list/button/delete}Delete selected grid items{/s}',
                        disabled: true,
                        action: 'deleteGridItems'
                    },
                    /* {/if} */
                    '->',
                    {
                        xtype: 'textfield',
                        name: 'searchfield',
                        action: 'searchGridItem',
                        width: 170,
                        cls: 'searchfield',
                        enableKeyEvents: true,
                        checkChangeBuffer: 500,
                        emptyText: '{s name=list/field/search}Search...{/s}'
                    },
                    { xtype: 'tbspacer', width: 6 }
                ]
            });
    },

    getPagingBar: function () {
        var me = this;
        return Ext.create('Ext.toolbar.Paging', {
            store: me.listStore,
            dock: 'bottom',
            displayInfo: true
        });
    },

    getGridSelModel: function () {
        return Ext.create('Ext.selection.CheckboxModel', {
            listeners: {
                // Unlocks the delete button if the user has checked at least one checkbox
                selectionchange: function (sm, selections) {
                    var owner = this.view.ownerCt,
                        btn = owner.down('button[action=deleteGridItems]');
                    btn.setDisabled(!selections.length);
                }
            }
        });
    },

    /**
   * title Renderer Method
   *
   * @param value
   */
    titleRenderer: function (value) {
        return Ext.String.format('{literal}<strong style="font-weight: 700;">{0}</strong>{/literal}', value);
    },

    /**
     * Renderer for the active flag
     *
     * @param value
     */
    activeColumnRenderer: function(value) {
        if (value) {
            return '<div class="sprite-tick"  style="width: 25px; height: 25px;">&nbsp;</div>';
        } else {
            return '<div class="sprite-cross" style="width: 25px; height: 25px;">&nbsp;</div>';
        }
    }
});
