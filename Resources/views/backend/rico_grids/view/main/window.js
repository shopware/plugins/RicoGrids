//  {namespace name=backend/rico_grids/view/main/window}
Ext.define('Shopware.apps.RicoGrids.view.main.Window', {
    extend: 'Enlight.app.Window',

    layout: 'border',

    width: 990,

    height: '50%',

    alias: 'widget.rico-grid-main-window',
    eventAlias: 'rico-grid-main-window',
    title: '{s name="windowTitle"}{/s}',

    initComponent: function () {
        var me = this;

        me.items = [
            {
                xtype: 'rico-grid-main-list',
                listStore: me.listStore
            }
        ];

        me.callParent(arguments);
    }
});
