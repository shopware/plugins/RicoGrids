Ext.define('Shopware.apps.RicoGrids.controller.List', {
    extend: 'Ext.app.Controller',

    refs: [
        { ref: 'mainWindow', selector: 'rico-grid-main-window' },
        { ref: 'gridWindow', selector: 'rico-grid-grid-window' },
        { ref: 'gridList', selector: 'rico-grid-main-list' },
        { ref: 'settingsForm', selector: 'navigation-entry-configuration' },
        { ref: 'saveEntryButton', selector: 'button[action=saveDetail]' },
        { ref: 'categoryTree', selector: 'navigation-menu-tree' },
        { ref: 'deleteButton', selector: 'navigation-menu-tree button[action=deleteEntry]' },
        { ref: 'duplicateButton', selector: 'navigation-menu-tree button[action=duplicateEntry]' }
    ],

    snippets: {
        duplicate: {
            infoText: 'ABCDEFG - INFOTEXT'
        },
        delete: {
            headline: 'Delete entries',
            text: 'Are you sure you want to delete grid: [0] and all its entries?',
            multiple: {
                success: {
                    headline: 'DELETE',
                    content: 'Entries are deleted'
                },
                failure: {
                    headline: 'ERROR',
                    content: 'Entries couldn\'t be deleted'
                }
            },
            growlMessage: 'growlMessage'
        }
    },

    init: function() {
        var me = this;

        me.control({
            // Context Menu
            'rico-grid-main-list': {
                'editGrid': me.onEditGrid,
                'deleteGrid': me.onDeleteGrid
            },
            'rico-grid-main-list textfield[action=searchGridItem]': {
                change: me.onGridSearch
            },
            'rico-grid-main-list button[action=addGrid]': {
                click: me.onAddGrid
            },
            'rico-grid-main-list button[action=deleteGridItems]': {
                click: me.onDeleteGridItems
            }
        });

        me.callParent(arguments);
    },

    onAddGrid: function() {
        var me = this;
        me.getView('Shopware.apps.RicoGrids.view.grid.Window').create({
            entryTemplateStore: me.subApplication.entryTemplateStore.load(),
            gridTemplateStore: me.subApplication.gridTemplateStore.load()
        }).show();
    },

    onEditGrid: function(view, rowIndex, colIndex, item) {
        var me = this,
            store = me.subApplication.gridStore,
            record = me.subApplication.listStore.getAt(rowIndex),
            entryListStore = me.subApplication.entryListStore;

        store.getProxy().extraParams = { id: record.get('id') };

        store.load({
            scope: this,
            callback: function(records, operation) {
                if (operation.success !== true || !records.length) {
                    return;
                }

                entryListStore.getProxy().extraParams = {
                    grid: record.get('id')
                };

                me.detailRecord = records[0];
                me.getView('Shopware.apps.RicoGrids.view.grid.Window').create({
                    record: me.detailRecord,
                    gridStore: store,
                    entryListStore: entryListStore.load(),
                    entryTemplateStore: me.subApplication.entryTemplateStore.load(),
                    gridTemplateStore: me.subApplication.gridTemplateStore.load()
                }).show();
                me.getController('Shopware.apps.RicoGrids.controller.Grid').onWindowLoad(me.getGridWindow());
            }
        });
    },

    onGridSearch: function(field, value) {
        var me = this,
            searchString = Ext.String.trim(value),
            store = me.subApplication.listStore;
        store.filters.clear();
        store.currentPage = 1;
        store.filter('name', searchString);
    },

    onDeleteGridItems: function() {
        var me = this,
            gridList = me.getGridList(),
            selectionModel = gridList.getSelectionModel(),
            selection = selectionModel.getSelection(),
            noOfElements = selection.length,
            store = me.subApplication.listStore;
        store.currentPage = 1;

        // Get the user to confirm the delete process

        Ext.MessageBox.confirm(
            me.snippets.delete.headline,
            Ext.String.format(me.snippets.delete.text, noOfElements), function (response) {
                if (response !== 'yes') {
                    return false;
                }
                if (noOfElements > 0) {
                    for (var i = 0; i < noOfElements; i++) {
                        me.deleteRecord(selection[i], store);
                    }
                }
            });
    },

    onDeleteGrid: function(view, rowIndex, colIndex, item) {
        var me = this,
            store = me.subApplication.listStore,
            record = store.getAt(rowIndex);
        store.currentPage = 1;

        // view.setLoading(true);
        Ext.MessageBox.confirm(
            me.snippets.delete.headline,
            Ext.String.format(me.snippets.delete.text, record.get('name')),
            function (response) {
                if (response !== 'yes') {
                    return false;
                }
                me.deleteRecord(record, store);
            }
        );
    },

    deleteRecord(record, store) {
        var me = this;

        record.destroy({
            callback: function(self, operation) {
                if (operation.success === true) {
                    Shopware.Notification.createGrowlMessage(me.snippets.delete.multiple.success.headline,
                        me.snippets.delete.multiple.success.content);
                    store.load();
                } else {
                    Shopware.Notification.createGrowlMessage(me.snippets.delete.multiple.failure.headline,
                        me.snippets.delete.multiple.failure.content);
                }
            }
        });
    }

});
