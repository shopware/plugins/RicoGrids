Ext.define('Shopware.apps.RicoGrids.controller.Grid', {
    extend: 'Ext.app.Controller',

    refs: [
        { ref: 'mainWindow', selector: 'rico-grid-grid-window' },
        { ref: 'configurationFieldset', selector: 'rico-grid-grid-tab-configuration' },
        { ref: 'entryListPanel', selector: 'rico-grid-grid-tab-entry-list' }
    ],

    snippets: {
        growlMessage: 'growlMessage',
        fields: {
            unset: 'Field unset',
            shopField: {
                unset: 'Field shop is unset'
            },
            gridNameField: {
                unset: 'Field name is unset'
            },
            gridDescription: {
                unset: 'Field description is unset'
            },
            gridTypeField: {
                unset: 'Field type is unset'
            },
            gridSubTypeField: {
                unset: 'Field type is unset'
            },
            frontendLayoutField: {
                unset: 'Field frontendLayout is unset'
            },
            gridLayoutField: {
                unset: 'Field gridLayout is unset'
            }
        },
        save: {
            success: {
                headline: 'SAVED',
                content: 'Saved the entry'
            },
            failure: {
                headline: 'ERROR',
                content: 'Coudln\'t save the entry'
            }
        }
    },

    init: function() {
        var me = this;

        me.control({
            'rico-grid-grid-window': {
                'windowLoaded': me.onWindowLoad,
                'tabChange': me.onTabChange,
                'saveDetail': me.onSave
            },
            'rico-grid-grid-tab-configuration': {
                'gridTypeSelected': me.onTypeSelection,
                'gridSubTypeSelected': me.onSubTypeSelection
            }
        });

        me.callParent(arguments);
    },

    onWindowLoad: function (mainWindow) {
        var me = this,
            configFieldset = me.getConfigurationFieldset(),
            configTranslation = configFieldset.getPlugin('translation'),
            mainForm = mainWindow.formPanel,
            form = mainForm.getForm(),
            shopField = form.findField('gridShop'),
            activeField = form.findField('gridActive'),
            gridNameField = form.findField('gridName'),
            gridDescription = form.findField('gridDescription'),
            gridTypeField = form.findField('gridType'),
            gridSubTypeField = form.findField('gridSubType'),
            frontendLayoutField = form.findField('frontendLayout'),
            gridLayoutField = form.findField('gridLayout'),
            orderField = form.findField('orderField');

        frontendLayoutField.setVisible(false);
        gridSubTypeField.setVisible(false);

        if (mainWindow.record) {
            if (mainWindow.record.get('type') === 'internal' || mainWindow.record.get('subType') === 'internal') {
                if (mainWindow.record.get('frontendLayout') != null) {
                    me.selectCombo(frontendLayoutField,
                        mainWindow.record.get('frontendLayout').id);
                }
                frontendLayoutField.setVisible(true);
            }
            if (mainWindow.record.get('type') === 'accordion') {
                me.selectCombo(gridSubTypeField, mainWindow.record.get('subType'));
                gridSubTypeField.setVisible(true);
            }
            me.selectCombo(gridLayoutField, mainWindow.record.get('gridLayout').id);
            me.selectCombo(shopField, mainWindow.record.get('shop')[0].id);
            gridNameField.setValue(mainWindow.record.get('name'))
                .setRawValue(mainWindow.record.get('name'));
            gridDescription.setValue(mainWindow.record.get('description'))
                .setRawValue(mainWindow.record.get('description'));
            me.selectCombo(gridTypeField, mainWindow.record.get('type'));
            me.selectCombo(orderField,
                mainWindow.record.get('orderType'));
            activeField.setValue(mainWindow.record.get('active'));

            if (configTranslation) {
                configTranslation.translationKey = mainWindow.record.get('id');
                configTranslation.init(configFieldset);
                configTranslation.enable();
            }
            configFieldset.items.map.gridAttributeForm.loadAttribute(mainWindow.record.get('id'), function () {
                configFieldset.items.map.gridAttributeForm.setHeight(configFieldset.items.map.gridAttributeForm.fieldSet.getHeight());
            });
        }
    },

    /**
     *  Selects an combo, make it visible and puts the values in their place
     * @param combo
     * @param type
     */
    selectCombo: function(combo, type) {
        var store = combo.store,
            valueField = combo.valueField,
            displayField = combo.displayField,
            recordNumber = store.findExact(valueField, type, 0);
        if (recordNumber === -1) { return -1; }

        var displayValue = store.getAt(recordNumber).data[displayField];
        combo.setValue(type);
        combo.setRawValue(displayValue);
        combo.selectedIndex = recordNumber;
        return recordNumber;
    },

    /**
     * Shows the combos if type = "internal"
     * @param call
     * @param combo
     */
    onTypeSelection: function(call, combo) {
        var form = call.getForm(),
            selected = combo.value;

        if (selected === 'internal') {
            form.findField('frontendLayout').setVisible(true);
        } else if (selected === 'accordion') {
            form.findField('gridSubType').setVisible(true);
        }
    },

    onSubTypeSelection: function(call, combo) {
        var form = call.getForm(),
            selected = combo.value;

        if (selected === 'internal') {
            form.findField('frontendLayout').setVisible(true);
        } else {
            form.findField('frontendLayout').setVisible(false);
        }
    },

    onTabChange: function(tabPanel, newCard, oldCard) {
        var me = this;

        if (newCard.xtype === 'rico-grid-grid-tab-entry-list') {
            me.getMainWindow().formPanel.dockedItems.items[0].setDisabled(true);
            me.getMainWindow().formPanel.dockedItems.items[0].setVisible(false);
        } else {
            me.getMainWindow().formPanel.dockedItems.items[0].setDisabled(false);
            me.getMainWindow().formPanel.dockedItems.items[0].setVisible(true);
        }
    },

    onSave: function (mainWindow) {
        var me = this,
            configFieldset = me.getConfigurationFieldset(),
            mainForm = mainWindow.formPanel,
            form = mainForm.getForm(),
            fields = me.saveHandle(form),
            store = me.subApplication.gridStore,
            entryListStore = me.subApplication.entryListStore,
            gridItem = mainWindow.record,
            isNew = false;

        if (gridItem) {
            configFieldset.items.map.gridAttributeForm.saveAttribute(gridItem.getId());
        }

        if (!gridItem) {
            store.getProxy().extraParams = { };
            gridItem = Ext.create('Shopware.apps.RicoGrids.model.Grid', {});
            isNew = true;
        }
        gridItem.set('name', fields.name);
        gridItem.set('description', fields.description);
        gridItem.set('shop', fields.shop);
        gridItem.set('type', fields.type);
        gridItem.set('subType', fields.subType);
        gridItem.set('gridLayout', fields.gridLayout);
        gridItem.set('frontendLayout', fields.frontendLayout);
        gridItem.set('orderType', fields.orderType);
        gridItem.set('active', fields.active);

        gridItem.save({
            callback: function (data, operation) {
                if (operation.success) {
                    me.growMessage(
                        me.snippets.save.success.headline,
                        me.snippets.save.success.content
                    );

                    if (isNew) {
                        store.getProxy().extraParams = { id: data.get('id') };
                        me.getMainWindow().destroy();
                        store.load({
                            scope: this,
                            callback: function(records, operation) {
                                if (operation.success !== true || !records.length) {
                                    window.console.error('Grid couldn\'t be loaded',
                                        operation, records);
                                    return;
                                }

                                entryListStore.getProxy().extraParams = {
                                    grid: records[0].get('id')
                                };

                                me.detailRecord = records[0];
                                me.getView('Shopware.apps.RicoGrids.view.grid.Window').create({
                                    record: me.detailRecord,
                                    gridStore: store,
                                    entryListStore: entryListStore.load(),
                                    entryTemplateStore: me.subApplication.entryTemplateStore.load(),
                                    gridTemplateStore: me.subApplication.gridTemplateStore.load()
                                }).show();
                            }
                        });
                    } else {
                        me.subApplication.listStore.load();
                        store.getProxy().extraParams = { 'id': data.getId() };
                        entryListStore.load();
                        store.load();
                        mainWindow.record = data;
                        me.onWindowLoad(mainWindow);
                    }
                } else {
                    me.growMessage(
                        me.snippets.save.failure.headline,
                        me.snippets.save.failure.content
                    );
                }
            }
        });
    },

    saveHandle: function (form) {
        var me = this,
            fields = {
                shop: form.findField('gridShop'),
                name: form.findField('gridName'),
                description: form.findField('gridDescription'),
                type: form.findField('gridType'),
                subType: form.findField('gridSubType'),
                frontendLayout: form.findField('frontendLayout'),
                gridLayout: form.findField('gridLayout'),
                active: form.findField('gridActive'),
                orderType: form.findField('orderField')
            },
            shops = {},
            shopCount;

        if (!me.checkFilled(fields)) {
            return;
        }

        for (shopCount = 0; shopCount < fields.shop.getValue().length; shopCount++) {
            shops[shopCount] = { 'id': fields.shop.getValue()[shopCount] };
        }

        var gridLayout = { 'id': fields.gridLayout.getValue() };
        if (gridLayout.id < 1) {
            gridLayout = fields.gridLayout.getRawValue();
        }

        var frontendLayout = fields.frontendLayout.getValue();
        if (fields.type.getValue() !== 'internal') {
            frontendLayout = null;
        }

        return {
            name: fields.name.getValue(),
            description: fields.description.getValue(),
            shop: shops,
            type: fields.type.getValue(),
            subType: fields.subType.getValue(),
            gridLayout: gridLayout,
            frontendLayout: frontendLayout,
            active: fields.active.getValue(),
            orderType: fields.orderType.getValue()
        };
    },

    checkFilled: function(fields) {
        var me = this;
        if (!fields.name.getValue()) {
            me.growMessage(
                me.snippets.fields.unset,
                me.snippets.fields.gridNameField.unset
            );
            return false;
        }
        if (!fields.description.getValue()) {
            me.growMessage(
                me.snippets.fields.unset,
                me.snippets.fields.gridDescription.unset
            );
            return false;
        }
        if (!fields.type.getValue()) {
            me.growMessage(
                me.snippets.fields.unset,
                me.snippets.fields.gridTypeField.unset
            );
            return false;
        }
        if (fields.type.getValue() === 'accordion') {
            if (!fields.subType.getValue()) {
                me.growMessage(
                    me.snippets.fields.unset,
                    me.snippets.fields.gridSubTypeField.unset
                );
                return false;
            }
        }
        if (!fields.shop.getValue()) {
            me.growMessage(
                me.snippets.fields.unset,
                me.snippets.fields.shopField.unset
            );
            return false;
        }
        if (!fields.gridLayout.getValue()) {
            me.growMessage(
                me.snippets.fields.unset,
                me.snippets.fields.gridLayoutField.unset
            );
            return false;
        }
        if (fields.gridLayout.getValue() === 'internal' &&
            !fields.frontendLayout.getValue()) {
            me.growMessage(
                me.snippets.fields.unset,
                me.snippets.fields.frontendLayoutField.unset
            );
            return false;
        }
        return true;
    },

    growMessage: function (title, responseText) {
        var me = this;
        Shopware.Notification.createGrowlMessage(title,
            responseText,
            me.snippets.growlMessage, '', false);
    }
});
