Ext.define('Shopware.apps.RicoGrids.controller.EntryList', {
    extend: 'Ext.app.Controller',

    types: {
        gridTypes: {
            internal: 'internal',
            external: 'external',
            lightbox: 'lightbox',
            placeholder: 'placeholder',
            accordion: 'accordion'
        }
    },

    refs: [
        { ref: 'mainWindow', selector: 'rico-grid-grid-window' },
        { ref: 'entryListPanel', selector: 'rico-grid-grid-tab-entry-list' },
        { ref: 'settingsForm', selector: 'rico-grid-grid-tab-configuration' }
    ],

    snippets: {
        delete: {
            messageBox: {
                headline: 'Delete the entry',
                text: 'Do you really want to delete the entry?'
            },
            success: {
                headline: 'DELETED',
                content: 'The entry was deleted'
            },
            failure: {
                headline: 'ERROR',
                content: 'Couldn\'t delete the entry'
            }
        },
        save: {
            success: {
                headline: 'SAVED',
                content: 'Saved the entry'
            },
            failure: {
                headline: 'ERROR',
                content: 'Coudln\'t save the entry'
            }
        },
        create: {
            success: {
                headline: 'Created the entry',
                content: 'The entry was created'
            },
            failure: {
                headline: 'ERROR',
                content: 'Coudln\'t create the entry'
            }
        },
        grid: {
            notFound: {
                headline: 'Couldn\'t find the desired grid',
                content: 'Please reload the window'
            }
        }
    },

    init: function() {
        var me = this;

        me.control({
            'rico-grid-grid-tab-entry-list': {
                'updateList': me.onUpdateList,
                'entryListSelectionChange': me.onSelectionChange,
                'deleteEntryListItem': me.onDeleteEntry
            },
            'rico-grid-grid-tab-entry-list button[action=addGridEntry]': {
                click: me.onAddEntry
            },
            'rico-grid-grid-tab-entry-list button[action=deleteGridEntries]': {
                click: me.onDeleteEntries
            },
            'rico-grid-grid-tab-entry-list textfield[action=searchEntries]': {
                change: me.onSearch
            },
            'rico-grid-grid-tab-entry-list button[action=saveSidebar]': {
                click: me.onSaveSidebar
            },
            'rico-grid-grid-tab-entry-list button[action=addEntryText]': {
                click: me.onAddTextTab
            },
            'rico-grid-grid-tab-entry-list button[action=deleteTab]': {
                click: me.onDeleteTextTab
            }
        });

        me.callParent(arguments);
    },

    onUpdateList: function(editor, context) {
        var me = this,
            record = context.record,
            store = me.subApplication.entryListStore,
            newValues = {
                position: context.newValues.position,
                name: context.newValues.name,
                active: context.newValues.active
            },
            oldValues = {
                position: context.originalValues.position,
                name: context.originalValues.name,
                active: context.originalValues.active
            };
        if (newValues.active !== oldValues.active) {
            record.set('active', newValues.active);
        }
        if (newValues.name !== oldValues.name) {
            record.set('name', newValues.name);
        }
        if (newValues.position !== oldValues.position) {
            record.set('position', newValues.position);
        }
        if (record.get('image') != null && record.get('image').hasOwnProperty('id')) {
            record.set('image', { 'id': record.get('image').id });
        } else {
            record.set('image', null);
        }
        if (record.get('detailImage') != null && record.get('detailImage').hasOwnProperty('id')) {
            record.set('detailImage', { 'id': record.get('detailImage').id });
        } else {
            record.set('detailImage', null);
        }

        record.save({
            callback: function (data, operation) {
                if (operation.success) {
                    Shopware.Notification.createGrowlMessage(
                        me.snippets.save.success.headline,
                        me.snippets.save.success.content, '', false);
                    store.load();
                } else {
                    Shopware.Notification.createGrowlMessage(
                        me.snippets.save.failure.headline,
                        me.snippets.save.failure.content, '', false);
                }
            }
        });
    },

    onSelectionChange: function(selectionModel, selection) {
        var me = this,
            tabPanel = me.getEntryListPanel(),
            entryInfo = tabPanel.down('form'),
            configTranslation = entryInfo.getPlugin('translation'),
            infoForm = entryInfo.getForm();

        if (selection.length <= 0) {
            entryInfo.disable();
            return;
        }
        var firstSelected = selection[0],
            nameField = infoForm.findField('nameField'),
            positionField = infoForm.findField('positionField'),
            mediaField = infoForm.findField('mediaField'),
            detailMediaField = infoForm.findField('detailMediaField'),
            bigField = infoForm.findField('bigField');

        infoForm.record = firstSelected;
        entryInfo.record = firstSelected;
        nameField.setRawValue(firstSelected.get('name'));
        nameField.setValue(firstSelected.get('name'));
        positionField.setRawValue(firstSelected.get('position'));
        positionField.setValue(firstSelected.get('position'));
        bigField.setValue(firstSelected.get('bigEntry'));
        if (firstSelected.get('image') != null && firstSelected.get('image').hasOwnProperty('id')) {
            mediaField.setValue(firstSelected.get('image').id);
        } else {
            mediaField.setValue(null);
        }
        if (firstSelected.get('detailImage') != null && firstSelected.get('detailImage').hasOwnProperty('id')) {
            detailMediaField.setValue(firstSelected.get('detailImage').id);
        } else {
            detailMediaField.setValue(null);
        }
        me.setGridType(entryInfo, infoForm, firstSelected);
        entryInfo.enable();

        if (configTranslation) {
            configTranslation.translationKey = firstSelected.get('id');
            configTranslation.init(entryInfo);
            configTranslation.initTranslationFields(entryInfo);
        }

        entryInfo.items.map.gridEntryAttributeForm.loadAttribute(firstSelected.get('id'), function () {
            entryInfo.items.map.gridEntryAttributeForm.setHeight(
                entryInfo.items.map.gridEntryAttributeForm.fieldSet.getHeight()
            );
        });
    },

    onDeleteEntries: function() {
        var me = this,
            tabPanel = me.getEntryListPanel(),
            gridList = tabPanel.down('grid'),
            selectionModel = gridList.getSelectionModel(),
            selection = selectionModel.getSelection(),
            noOfElements = selection.length,
            store = me.subApplication.entryListStore;
        store.currentPage = 1;
        Ext.MessageBox.confirm(
            me.snippets.delete.headline,
            Ext.String.format(me.snippets.delete.text, noOfElements), function (response) {
                if (response !== 'yes') {
                    return false;
                }
                if (noOfElements > 0) {
                    for (var i = 0; i < noOfElements; i++) {
                        me.deleteRecord(selection[i], store);
                    }
                }
            });
    },

    onDeleteEntry: function (view, rowIndex, colIndex, item) {
        var me = this,
            store = me.subApplication.entryListStore,
            record = store.getAt(rowIndex);

        Ext.MessageBox.confirm(
            me.snippets.delete.messageBox.headline,
            Ext.String.format(me.snippets.delete.messageBox.text, record.get('name')),
            function (response) {
                if (response !== 'yes') {
                    return false;
                }
                me.deleteRecord(record, store);
            }
        );
    },

    deleteRecord(record, store) {
        var me = this;
        record.destroy({
            callback: function(self, operation) {
                if (operation.success === true) {
                    Shopware.Notification.createGrowlMessage(
                        me.snippets.delete.success.headline, me.snippets.delete.success.content);
                    store.load();
                } else {
                    Shopware.Notification.createGrowlMessage(
                        me.snippets.delete.success.headline, me.snippets.delete.success.content);
                }
            }
        });
    },

    onAddEntry: function() {
        var me = this,
            mainWindow = me.getMainWindow(),
            store = me.subApplication.entryListStore,
            position = store.totalCount;

        if (mainWindow.record) {
            var gridRecord = mainWindow.record,
                gridId = gridRecord.getId();
        } else {
            Shopware.Notification.createGrowlMessage(
                me.snippets.grid.notFound.headline, me.snippets.grid.notFound.message, '', false);
            return;
        }

        Ext.Msg.prompt('{s name=view/add/dialog/headline}Enter entry name{/s}',
            '{s name=view/add/dialog/label}Entry name{/s}', function (btn, text) {
                if (btn == 'ok') {
                    if (text == '') {
                        me.onAddEntry();
                    } else {
                        if (!gridId || gridId == 0) {
                            Shopware.Notification.createGrowlMessage(
                                me.snippets.grid.notFound.headline, me.snippets.grid.notFound.message, '', false);
                            return;
                        }
                        // me.subApplication.entryListStore.getProxy().extraParams = { 'grid': gridId };
                        store.getProxy().extraParams = { 'grid': gridId };

                        var gridEntry = Ext.create('Shopware.apps.RicoGrids.model.EntryList',
                            {
                                'name': text,
                                'detailName': '',
                                'grid': { 'id': gridId },
                                'subHeading': '',
                                'text': '',
                                'link': '',
                                'bigEntry': false,
                                'image': null,
                                'detailImage': null,
                                'position': position,
                                'active': false
                            });

                        gridEntry.save({
                            callback: function (record, operation) {
                                if (operation.success) {
                                    Shopware.Notification.createGrowlMessage(
                                        me.snippets.create.success.headline,
                                        me.snippets.create.success.content, '', false);
                                    store.load();
                                } else {
                                    Shopware.Notification.createGrowlMessage(
                                        me.snippets.create.failure.headline,
                                        me.snippets.create.failure.content, '', false);
                                }
                            }
                        });
                        me.subApplication.entryListStore.load();
                    }
                }
            });
    },

    onSaveSidebar: function () {
        var me = this,
            tabPanel = me.getEntryListPanel(),
            entryInfo = tabPanel.down('form'),
            infoForm = entryInfo.getForm(),
            formRecord = infoForm.record,
            store = me.subApplication.entryListStore,
            record = store.getById(formRecord.getId()),
            descTabPanel = entryInfo.getComponent('descTabPanel'),
            tabs = descTabPanel.items.items.length;

        record.set('name', infoForm.findField('nameField').getValue());
        record.set('position', infoForm.findField('positionField').getValue());
        record.set('detailName', infoForm.findField('detailNameField').getValue());
        record.set('bigEntry', infoForm.findField('bigField').getValue());
        if (infoForm.findField('mediaField').getValue()) {
            record.set('image', { 'id': infoForm.findField('mediaField').getValue() });
        } else {
            record.set('image', null);
        }
        if (infoForm.findField('detailMediaField').getValue()) {
            record.set('detailImage', { 'id': infoForm.findField('detailMediaField').getValue() });
        } else {
            record.set('detailImage', null);
        }

        if (record.get('grid').type === me.types.gridTypes.external ||
          record.get('grid').subType === me.types.gridTypes.external) {
            record.set('link', infoForm.findField('linkField').getValue());
        } else if (record.get('grid').type === me.types.gridTypes.lightbox ||
          record.get('grid').subType === me.types.gridTypes.lightbox) {
            record.set('subHeading', infoForm.findField('subHeadingField').getValue());
            record.set('text', me.onSaveTabHandle(infoForm, tabs));
        } else if (record.get('grid').type === me.types.gridTypes.internal ||
          record.get('grid').subType === me.types.gridTypes.internal) {
            record.set('subHeading', infoForm.findField('subHeadingField').getValue());
            record.set('text', me.onSaveTabHandle(infoForm, tabs));
        }
        if (record.get('grid').type === me.types.gridTypes.accordion) {
            record.set('accordion', infoForm.findField('accordionField').getValue());
        }

        entryInfo.items.map.gridEntryAttributeForm.saveAttribute(record.getId());

        record.save({
            callback: function (data, operation) {
                if (operation.success) {
                    Shopware.Notification.createGrowlMessage(me.snippets.save.success.headline,
                        me.snippets.save.success.content, '', false);
                    store.load();
                } else {
                    Shopware.Notification.createGrowlMessage(me.snippets.save.failure.headline,
                        me.snippets.save.failure.headline, '', false);
                }
            }
        });
    },

    onSearch: function (field, value) {
        var me = this,
            searchString = Ext.String.trim(value),
            store = me.subApplication.entryListStore;
        store.filters.clear();
        store.currentPage = 1;
        store.filter('name', searchString);
    },

    onAddTextTab: function () {
        var me = this,
            tabPanel = me.getEntryListPanel(),
            entryInfo = tabPanel.down('form'),
            descTabPanel = entryInfo.getComponent('descTabPanel'),
            tabs = descTabPanel.items.items.length;
        descTabPanel.add({
            title: 'Text: ' + tabs,
            xtype: 'tinymce',
            name: 'textField/' + tabs,
            itemId: 'textField/' + tabs
        }
        );
    },

    onDeleteTextTab: function () {
        var me = this,
            tabPanel = me.getEntryListPanel(),
            entryInfo = tabPanel.down('form'),
            activeTab = entryInfo.getComponent('descTabPanel').getActiveTab();
        if (activeTab) {
            activeTab.destroy();
        }
    },

    setGridType(entryInfo, infoForm, firstSelected, subGridType = '') {
        var me = this,
            gridType = subGridType !== '' ? subGridType : firstSelected.get('grid').type;
        if (gridType === me.types.gridTypes.external) {
            infoForm.findField('linkField').setVisible(true);
            infoForm.findField('linkField').setValue(firstSelected.get('link'));
        } else if (gridType === me.types.gridTypes.lightbox) {
            infoForm.findField('subHeadingField').setVisible(true);
            infoForm.findField('subHeadingField')
                .setValue(firstSelected.get('subHeading'));
            entryInfo.getComponent('descTabPanel').setVisible(true);
            me.onSelectionTabHandle(entryInfo, firstSelected);
        } else if (gridType === me.types.gridTypes.internal) {
            infoForm.findField('subHeadingField').setVisible(true);
            infoForm.findField('subHeadingField').setValue(firstSelected.get('subHeading'));
            infoForm.findField('detailNameField').setVisible(true);
            infoForm.findField('detailNameField').setValue(firstSelected.get('detailName'));
            infoForm.findField('detailMediaField').setVisible(true);
            if (firstSelected.get('detailImage') != null && firstSelected.get('detailImage').hasOwnProperty('id')) {
                infoForm.findField('detailMediaField').setValue(firstSelected.get('detailImage').id);
            }
            entryInfo.getComponent('descTabPanel').setVisible(true);
            me.onSelectionTabHandle(entryInfo, firstSelected);
        } else if (gridType === me.types.gridTypes.accordion) {
            infoForm.findField('accordionField').setVisible(true);
            infoForm.findField('accordionField').setValue(firstSelected.get('accordion'));
            if (firstSelected.get('grid').subType !== '') {
                me.setGridType(entryInfo, infoForm, firstSelected, firstSelected.get('grid').subType);
            }
        }
    },

    onSelectionTabHandle: function (entryInfo, firstSelected) {
        var me = this,
            descTabPanel = entryInfo.getComponent('descTabPanel'),
            newtabPanel = Ext.create('Ext.tab.Panel', {
                items: me.getDescriptionTabs(firstSelected),
                dockedItems: {
                    xtype: 'toolbar',
                    dock: 'top',
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'sprite-plus-circle',
                            text: '{s name=entry/sidebar/desc/tab/add}Add new text{/s}',
                            action: 'addEntryText'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'sprite-minus-circle',
                            text: '{s name=entry/sidebar/desc/tab/remove}remove this tab{/s}',
                            action: 'deleteTab'
                        }
                    ]
                },
                split: false,
                hidden: true,
                id: 'descTabPanel',
                flex: 1,
                activeTab: 0
            });
        descTabPanel.destroy();
        entryInfo.add(newtabPanel);
        entryInfo.getComponent('descTabPanel').setVisible(true);
    },

    onSaveTabHandle: function (infoForm, tabs) {
        var text = [],
            i = 0;
        if (tabs > 0) {
            for (i = 0; i <= tabs - 1; i++) {
                if (infoForm.findField('textField/' + i)) {
                    text.push(
                        { 'text': infoForm.findField('textField/' + i).getValue() });
                }
            }
        }
        return text;
    },

    getDescriptionTabs: function(firstSelected) {
        var items = [];
        if (firstSelected.get('text') && firstSelected.get('text').length > 0) {
            for (var cnt = 0; cnt <= firstSelected.get('text').length - 1; cnt++) {
                var title = 'Text: ' + cnt,
                    name = 'textField/' + cnt,
                    content = firstSelected.get('text')[cnt].text;
                items.push({
                    title: title,
                    xtype: 'tinymce',
                    name: name,
                    itemId: name,
                    value: content,
                    rawValue: content,
                    translatable: true,
                    translationName: name
                });
            }
        }
        return items;
    }

});
