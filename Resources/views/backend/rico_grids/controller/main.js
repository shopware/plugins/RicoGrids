//  {namespace name=backend/rico_grids/controller/main}
Ext.define('Shopware.apps.RicoGrids.controller.Main', {
    extend: 'Enlight.app.Controller',

    init: function() {
        var me = this;
        me.handleStore(me);

        me.mainWindow = me.getView('main.Window').create({
            listStore: me.subApplication.listStore
        }).show();
    },

    handleStore: function(me) {
        me.subApplication.listStore = me.subApplication.getStore('List');
        me.subApplication.gridStore = me.subApplication.getStore('Grid');
        me.subApplication.entryListStore = me.subApplication.getStore('EntryList');
        me.subApplication.entryTemplateStore = me.subApplication.getStore('EntryTemplate');
        me.subApplication.gridTemplateStore = me.subApplication.getStore('GridTemplate');
        me.subApplication.shopStore = me.subApplication.getStore('Shopware.store.Shop');

        me.subApplication.listStore.load();
        me.subApplication.gridStore.load();
        me.subApplication.entryListStore.load();
        me.subApplication.entryTemplateStore.load();
        me.subApplication.gridTemplateStore.load();
        me.subApplication.shopStore.load();
    }
});
