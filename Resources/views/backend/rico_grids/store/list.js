Ext.define('Shopware.apps.RicoGrids.store.List', {
    extend: 'Ext.data.Store',

    remoteSort: true,
    remoteFilter: true,

    model: 'Shopware.apps.RicoGrids.model.List'
});
