Ext.define('Shopware.apps.RicoGrids.store.Grid', {
    extend: 'Ext.data.Store',

    model: 'Shopware.apps.RicoGrids.model.Grid'
});
