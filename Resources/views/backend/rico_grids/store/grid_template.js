Ext.define('Shopware.apps.RicoGrids.store.GridTemplate', {
    extend: 'Ext.data.Store',

    remoteSort: true,
    remoteFilter: true,

    model: 'Shopware.apps.RicoGrids.model.GridTemplate'
});
