Ext.define('Shopware.apps.RicoGrids.store.EntryTemplate', {
    extend: 'Ext.data.Store',

    remoteSort: true,
    remoteFilter: true,

    model: 'Shopware.apps.RicoGrids.model.EntryTemplate'
});
