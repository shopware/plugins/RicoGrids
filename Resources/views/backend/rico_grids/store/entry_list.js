Ext.define('Shopware.apps.RicoGrids.store.EntryList', {
    extend: 'Ext.data.Store',

    remoteSort: true,
    remoteFilter: true,

    model: 'Shopware.apps.RicoGrids.model.EntryList'
});
