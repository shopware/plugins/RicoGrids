Ext.define('Shopware.apps.RicoGrids', {
    extend: 'Enlight.app.SubApplication',

    name: 'Shopware.apps.RicoGrids',

    loadPath: '{url action=load}',
    bulkLoad: true,

    controllers: [
        'Shopware.apps.RicoGrids.controller.Main',
        'Shopware.apps.RicoGrids.controller.List',
        'Shopware.apps.RicoGrids.controller.Grid',
        'Shopware.apps.RicoGrids.controller.EntryList'
    ],

    views: [
        'Shopware.apps.RicoGrids.view.main.Window',
        'Shopware.apps.RicoGrids.view.main.List',

        'Shopware.apps.RicoGrids.view.grid.Window',

        'Shopware.apps.RicoGrids.view.grid.tab.Configuration',
        'Shopware.apps.RicoGrids.view.grid.tab.EntryList'
    ],

    models: [
        'Shopware.apps.RicoGrids.model.List',
        'Shopware.apps.RicoGrids.model.Grid',
        'Shopware.apps.RicoGrids.model.EntryList',
        'Shopware.apps.RicoGrids.model.EntryTemplate',
        'Shopware.apps.RicoGrids.model.GridTemplate'
    ],
    stores: [
        'Shopware.apps.RicoGrids.store.List',
        'Shopware.apps.RicoGrids.store.Grid',
        'Shopware.apps.RicoGrids.store.EntryList',
        'Shopware.apps.RicoGrids.store.EntryTemplate',
        'Shopware.apps.RicoGrids.store.GridTemplate',
        'Shopware.store.Shop'
    ],

    launch: function() {
        return this.getController('Shopware.apps.RicoGrids.controller.Main').mainWindow;
    }
});
