Ext.define('Shopware.apps.RicoGrids.model.GridTemplate', {

    extend: 'Shopware.data.Model',

    idProperty: 'id',

    fields: [
        { name: 'id', type: 'int', useNull: true },
        { name: 'name', type: 'string' }
    ],

    proxy: {
        type: 'ajax',

        /**
     * Configure the url mapping for the different
     * store operations based on
     * @object
     */
        api: {
            read: '{url controller=GridTemplate action=list}'
        },
        /**
     * Configure the data reader
     * @object
     */
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
