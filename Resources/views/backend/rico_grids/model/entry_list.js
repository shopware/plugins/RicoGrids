Ext.define('Shopware.apps.RicoGrids.model.EntryList', {

    extend: 'Shopware.data.Model',

    idProperty: 'id',

    fields: [
        { name: 'id', type: 'int', useNull: true },
        { name: 'grid', type: 'object', useNull: true },
        { name: 'name', type: 'string', useNull: true },
        { name: 'accordion', type: 'string', useNull: true },
        { name: 'detailName', type: 'string', useNull: true },
        { name: 'subHeading', type: 'string', useNull: true },
        { name: 'text', type: 'auto', useNull: true },
        { name: 'link', type: 'string', useNull: true },
        { name: 'image', type: 'object', useNull: true },
        { name: 'detailImage', type: 'object', useNull: true },
        { name: 'bigEntry', type: 'boolean', useNull: true },
        { name: 'position', type: 'integer', useNull: true },
        { name: 'active', type: 'boolean', useNull: true }
    ],

    proxy: {
        type: 'ajax',

        /**
     * Configure the url mapping for the different
     * store operations based on
     * @object
     */
        api: {
            read: '{url controller=RicoGridEntry action=list}',
            create: '{url controller=RicoGridEntry action=create}',
            update: '{url controller=RicoGridEntry action=update}',
            destroy: '{url controller=RicoGridEntry action=delete}'
        },
        /**
     * Configure the data reader
     * @object
     */
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
