Ext.define('Shopware.apps.RicoGrids.model.List', {

    extend: 'Shopware.data.Model',

    idProperty: 'id',

    fields: [
        { name: 'id', type: 'int', useNull: true },
        { name: 'name', type: 'string' },
        { name: 'type', type: 'string' },
        { name: 'active', type: 'boolean' }
    ],

    proxy: {
        type: 'ajax',

        /**
     * Configure the url mapping for the different
     * store operations based on
     * @object
     */
        api: {
            read: '{url controller=RicoGrids action=list}',
            create: '{url controller=RicoGrids action=create}',
            destroy: '{url controller=RicoGrids action=delete}'
        },
        /**
     * Configure the data reader
     * @object
     */
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
