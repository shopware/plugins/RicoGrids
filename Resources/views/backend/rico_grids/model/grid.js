Ext.define('Shopware.apps.RicoGrids.model.Grid', {

    extend: 'Shopware.data.Model',

    idProperty: 'id',

    fields: [
        { name: 'id', type: 'int', useNull: true },
        { name: 'name', type: 'string' },
        { name: 'description', type: 'string' },
        { name: 'type', type: 'string' },
        { name: 'subType', type: 'string' },
        { name: 'shop', type: 'object' },
        { name: 'frontendLayout', type: 'object' },
        { name: 'gridLayout', type: 'object' },
        { name: 'active', type: 'boolean' },
        { name: 'orderType', type: 'int' }
    ],

    proxy: {
        type: 'ajax',

        /**
     * Configure the url mapping for the different
     * store operations based on
     * @object
     */
        api: {
            read: '{url controller=RicoGrids action=detail}',
            create: '{url controller=RicoGrids action=create}',
            update: '{url controller=RicoGrids action=update}'
        },
        /**
     * Configure the data reader
     * @object
     */
        reader: {
            type: 'json',
            root: 'data'
        }
    }
});
