// {block name="backend/performance/view/main/multi_request_tasks" append}
Ext.define('Shopware.apps.Performance.view.main.Grid', {
    override: 'Shopware.apps.Performance.view.main.MultiRequestTasks',

    initComponent: function() {
        this.addProgressBar(
            {
                initialText: 'Grid URLs',
                progressText: '[0] of [1] grid URLs',
                requestUrl: '{url controller=RicoGrids action=seoRicoGrid}'
            },
            'grid',
            'seo'
        );

        this.callParent(arguments);
    }
});
// {/block}
