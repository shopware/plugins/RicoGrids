{extends file="frontend/rico_grid/index.tpl"}

{block name='frontend_index_content'}
    {$smarty.block.parent}
    {if $grid.gridLayout}
        {$gridLayout=$grid.gridLayout.name}
        {if "frontend/rico_grid/grid/$gridLayout.tpl"|template_exists}
            {include file="frontend/rico_grid/grid/$gridLayout.tpl"}
        {else}
            {include file="frontend/rico_grid/grid/default.tpl"}
        {/if}
    {else}
        {include file="frontend/rico_grid/grid/default.tpl"}
    {/if}
    {$gridAttributes.formular}
{/block}
