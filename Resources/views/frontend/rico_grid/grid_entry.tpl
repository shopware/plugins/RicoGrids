{extends file="frontend/rico_grid/index.tpl"}

{block name='frontend_index_content'}
    {$smarty.block.parent}

    {if $grid.frontendLayout}
        {$gridLayout=$grid.frontendLayout.name}
        {if "frontend/rico_grid/entry/$gridLayout.tpl"|template_exists}
            {include file="frontend/rico_grid/entry/$gridLayout.tpl"}
        {else}
            {include file="frontend/rico_grid/entry/default.tpl"}
        {/if}
    {else}
        {include file="frontend/rico_grid/entry/default.tpl"}
    {/if}
{/block}
