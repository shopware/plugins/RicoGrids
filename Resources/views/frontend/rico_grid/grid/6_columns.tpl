{extends file="frontend/rico_grid/grid/default.tpl"}

{block name="frontend/rico_grid/grid/wrapper-class"} rico-grid--{$gridLayout}{/block}

{block name="frontend/rico_grid/grid/container-style"}
    grid-column-gap: 0;
    grid-template-columns: repeat(6, {math equation="max / col" max=100 col=6 format="%.2f"}%);
    grid-template-rows: repeat({$rowAmount}, 210px);
{/block}

{block name="frontend/rico_grid/grid/default/box"}
    <div class="entry {$entry.gridEntry.name}{if $entry.gridEntry.isBigEntry} entry--big{/if}"
         style="grid-column: {$entry.coordinates.y.start} / {$entry.coordinates.y.end};
                 grid-row: {$entry.coordinates.x.start} / {$entry.coordinates.x.end};">
        <a href="{url controller=$grid.name|slugify action=$entry.gridEntry.name|slugify}"
           title={$entry.gridEntry.name}>
            <div class="entry-content">
                {if $entry.gridEntry.image}
                    <div class="entry-content--image">
                        {if $entry.gridEntry.image.thumbnails|@count > 0}
                            {block name="frontend_rico_grid_grid_6_columns_box_picture"}
                                <picture>
                                    <img srcset="{$entry.gridEntry.image.thumbnails[0].sourceSet}"
                                         alt="{$entry.gridEntry.name}" />
                                </picture>
                            {/block}
                        {else}
                            {block name="frontend_rico_grid_grid_6_columns_box_image"}
                                <img src="{$entry.gridEntry.image.source}" alt="{$entry.gridEntry.name}" />
                            {/block}
                        {/if}
                    </div>
                {/if}
            </div>
            <p>{$entry.gridEntry.name}</p>
        </a>
    </div>
{/block}
