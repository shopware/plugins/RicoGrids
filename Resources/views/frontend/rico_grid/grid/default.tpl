{block name="frontend/rico_grid/grid/default/wrapper"}
    <div class="rico-grid{block name="frontend/rico_grid/grid/wrapper-class"}rico-grid--default{/block}">
        {block name="frontend/rico_grid/grid/default/heading"}
            <div class="headline">
                <div class="grid--headline">
                    <h1>{$grid.name}</h1>
                </div>
                <div class="grid--subheadline">
                    <p>{$grid.description}</p>
                </div>
            </div>
        {/block}
        <div class="grid--container"
             style="{block name="frontend/rico_grid/grid/container-style"}
                grid-column-gap: 0;
                grid-template-columns: repeat(3, {math equation="max / col" max=100 col=3 format="%.2f"}%);
                grid-template-rows: repeat({$rowAmount}, 210px);
             {/block}">
            {foreach $entryMap as $entry}
                {block name="frontend/rico_grid/grid/default/box"}
                    <div class="entry {$entry.name}">
                        <div class="entry-content">
                            {if $entry.image}
                                <div class="entry-content--image">
                                    {if $entry.image.thumbnails|@count > 0}
                                        {block name="frontend/rico_grid/grid/default/box/picture"}
                                            <picture>
                                                <img srcset="{$entry.image.thumbnails[0].sourceSet}" alt="{$entry.name}" />
                                            </picture>
                                        {/block}
                                    {else}
                                        {block name="frontend/rico_grid/grid/default/box/image"}
                                            <img src="{$entry.image.source}" alt="{$entry.name}" />
                                        {/block}
                                    {/if}
                                </div>
                            {/if}
                            {foreach $entry.text as $content}
                                <div class="entry-content--text">
                                    {$content.text}
                                </div>
                            {/foreach}
                        </div>
                    </div>
                {/block}
            {/foreach}
        </div>
    </div>
{/block}
