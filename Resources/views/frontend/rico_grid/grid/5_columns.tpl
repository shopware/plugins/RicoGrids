{extends file="frontend/rico_grid/grid/6_columns.tpl"}


{block name="frontend/rico_grid/grid/container-style"}
    grid-column-gap: 0;
    grid-template-columns: repeat(5, {math equation="max / col" max=100 col=5 format="%.2f"}%);
    grid-template-rows: repeat({$rowAmount}, 210px);
{/block}