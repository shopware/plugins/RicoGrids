{foreach $gridEntries as $entry}
    <div class="entry {$entry.name}">
        <h1>{$entry.name}</h1>
        <h2>{$entry.subHeading}</h2>
        <div class="entry-content">
            {if $entry.image}
                <div class="entry-content--image">
                    {if $entry.image.thumbnails|@count > 0}
                        {block name='frontend/rico_grid/grid/3_columns/box/picture'}
                            <picture>
                                <img srcset="{$entry.image.thumbnails[0].sourceSet}" alt="{$entry.name}" />
                            </picture>
                        {/block}
                    {else}
                        {block name='frontend/rico_grid/grid/3_columns/box/image'}
                            <img src="{$entry.image.source}" alt="{$entry.name}" />
                        {/block}
                    {/if}
                </div>
            {/if}
            {foreach $entry.text as $content}
                <div class="entry-content--text">
                    {$content.text}
                </div>
            {/foreach}
        </div>
    </div>
{/foreach}
