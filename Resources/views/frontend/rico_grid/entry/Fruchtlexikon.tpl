<div class="grid--entry">
    <div class="headline">
        <div class="entry-title category--headline"><h1>{$gridEntry.name}</h1></div>
        <div class="entry-title--sub category--subheadline"><p>{$gridEntry.subHeading}</p></div>
    </div>
    <div class="entry-image">
        {if $gridEntry.detailImage}
            <div class="image">
                {if $gridEntry.detailImage.thumbnails|@count > 0}
                    {block name='rico_grids_entry_fruchtlexikon_picture'}
                        <picture>
                            <img srcset="{$gridEntry.detailImage.thumbnails[0].sourceSet}" alt="{$gridEntry.name}" />
                        </picture>
                    {/block}
                {else}
                    {block name='rico_grids_entry_fruchtlexikon_image'}
                        <img src="{$gridEntry.detailImage.source}" alt="{$gridEntry.name}" />
                    {/block}
                {/if}
            </div>
        {/if}
    </div>
    <div class="entry-text--wrapper">
        {foreach $gridEntry.text as $content}
            <div class="entry--text">
                {$content.text}
            </div>
        {/foreach}
    </div>
</div>
