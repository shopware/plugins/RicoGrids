<div class="grid--entry team">
    <div class="entry-image">
        {if $gridEntry.detailImage}
            <div class="image">
                {if $gridEntry.detailImage.thumbnails|@count > 0}
                    {block name='rico_grids_entry_team_picture'}
                        <picture>
                            <img srcset="{$gridEntry.detailImage.thumbnails[0].sourceSet}" alt="{$gridEntry.name}" />
                        </picture>
                    {/block}
                {else}
                    {block name='rico_grids_entry_team_image'}
                        <img src="{$gridEntry.detailImage.source}" alt="{$gridEntry.name}" />
                    {/block}
                {/if}
            </div>
        {/if}
    </div>
    <div class="inner--content-wrapper">
        <div class="entry-title">{$gridEntry.name}</div>
        <div class="entry-text--wrapper">
            {foreach $gridEntry.text as $content}
                {if $content@iteration == 1}
                    <div class="entry--text">{$content.text}</div>
                {/if}
            {/foreach}
            <div class="accordion">
                <div class="bullet">
                    {foreach $gridEntry.text as $content}
                        {if $content@iteration == 2}
                            <div class="bullet--headline">
                                {$content.text}
                            </div>
                        {/if}
                        {if $content@iteration == 3}
                            <div class="bullet--content">
                                {$content.text}
                            </div>
                        {/if}
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
</div>
