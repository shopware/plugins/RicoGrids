<div class="grid--entry">
    <div class="entry-title">
        {$gridEntry.name}
    </div>
    <div class="entry-title--sub">
        {$gridEntry.subHeading}
    </div>
    <div class="entry-image">
        {if $gridEntry.detailImage}
            <div class="image">
                {if $gridEntry.detailImage.thumbnails|@count > 0}
                    {block name='rico_grids_entry_default_picture'}
                        <picture>
                            <img srcset="{$gridEntry.detailImage.thumbnails[0].sourceSet}" alt="{$gridEntry.name}" />
                        </picture>
                    {/block}
                {else}
                    {block name='rico_grids_default_team_image'}
                        <img src="{$gridEntry.detailImage.source}" alt="{$gridEntry.name}" />
                    {/block}
                {/if}
            </div>
        {/if}
    </div>
    <div class="entry-text--wrapper">
        {foreach $gridEntry.text as $content}
            <div class="entry--text">
                {$content.text}
            </div>
        {/foreach}
    </div>
</div>
