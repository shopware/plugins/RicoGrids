{extends file="frontend/index/index.tpl"}


{block name='frontend_index_left_categories'}
{/block}
{block name='frontend_index_content'}
    {if $response == 'error'}
        {include file="frontend/_includes/messages.tpl" type="error" content="Your content"}
    {elseif $response == 'warning'}
        {include file="frontend/_includes/messages.tpl" type="warning" content="Your content"}
    {/if}
{/block}