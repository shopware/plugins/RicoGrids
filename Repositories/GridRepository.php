<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids\Repositories;

use Doctrine\Common\Collections\ArrayCollection;
use RicoGrids\Models\Grid;
use Shopware\Components\Model\ModelRepository;

/**
 * Class GridRepository
 */
class GridRepository extends ModelRepository
{
    private $model = Grid::class;
    private $alias = 'grid';

    /**
     * @return ArrayCollection
     */
    public function findByDefined(array $params = [])
    {
        $builder = $this->createQueryBuilder('findByDefined')
            ->select($this->alias)
            ->from($this->model, $this->alias)
            ->innerJoin($this->alias . '.shop', 'shop')
            ->groupBy($this->alias . '.id');
        foreach ($params as $where => $expression) {
            if (is_array($expression)) {
                if (key_exists('prefix', $expression) &&
                $expression['prefix']) {
                    $builder->andWhere($this->alias . '.' . $where . '=' . $expression['value']);
                } else {
                    $builder->andWhere($where . '=' . $expression['value']);
                }
            } else {
                $builder->andWhere($this->alias . '.' . $where . '=' . $expression);
            }
        }
        $map = new ArrayCollection();
        foreach ($builder->getQuery()->getArrayResult() as $key => $item) {
            $map->add($this->getEntityManager()->getRepository($this->model)->find($item['id']));
        }

        return $map;
    }
}
