<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids\Repositories;

use RicoGrids\Models\GridEntry;
use Shopware\Components\Model\ModelRepository;

/**
 * Class GridEntryRepository
 */
class GridEntryRepository extends ModelRepository
{
    /**
     * class name
     *
     * @var string
     */
    private $model = GridEntry::class;

    /**
     * Model alias
     *
     * @var string
     */
    private $alias = 'grid_entry';

    /**
     * @param int $oldPosition
     * @param int $newPosition
     * @param int $gridId
     *
     * @return mixed
     */
    public function findToHigher($oldPosition, $newPosition, $gridId)
    {
        $builder = $this->createQueryBuilder('findLower');
        $builder->select($this->alias)
            ->from($this->model, $this->alias)
            ->where($this->alias . '.position > :oldPosition')
            ->andWhere($this->alias . '.position <= :newPosition')
            ->andWhere($this->alias . '.id != :gridId')
            ->setParameter('oldPosition', $oldPosition)
            ->setParameter('newPosition', $newPosition)
            ->setParameter('gridId', $gridId);

        return $builder->getQuery()->execute();
    }

    /**
     * @param int $newPosition
     * @param int $oldPosition
     * @param int $gridId
     *
     * @return mixed
     */
    public function findToLower($newPosition, $oldPosition, $gridId)
    {
        $builder = $this->createQueryBuilder('findLower');
        $builder->select($this->alias)
            ->from($this->model, $this->alias)
            ->where($this->alias . '.position < :oldPosition')
            ->andWhere($this->alias . '.position >= :newPosition')
            ->andWhere($this->alias . '.id != :gridId')
            ->setParameter('oldPosition', $oldPosition)
            ->setParameter('newPosition', $newPosition)
            ->setParameter('gridId', $gridId);

        return $builder->getQuery()->execute();
    }
}
