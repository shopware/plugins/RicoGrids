<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids\Models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Shopware\Components\Model\ModelEntity;
use Shopware\Models\Media\Media;

/**
 * @ORM\Entity(repositoryClass="RicoGrids\Repositories\GridEntryRepository")
 * @ORM\Table(name="rico_grid_entry")
 */
class GridEntry extends ModelEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id = null;

    /**
     * @var Grid
     * @ORM\ManyToOne(targetEntity="RicoGrids\Models\Grid", inversedBy="gridEntries")
     */
    protected $grid;

    /**
     * @var string
     * @ORM\Column()
     */
    protected $name = '';

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    protected $detailName = '';

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    protected $subHeading = '';

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    protected $accordion = '';

    /**
     * @var Media
     * @ORM\ManyToOne(targetEntity="Shopware\Models\Media\Media")
     * @ORM\JoinColumn(name="image", referencedColumnName="id", nullable=true)
     */
    protected $image;

    /**
     * @var Media
     * @ORM\ManyToOne(targetEntity="Shopware\Models\Media\Media")
     * @ORM\JoinColumn(name="detail_image", referencedColumnName="id", nullable=true)
     */
    protected $detailImage;

    /** @var array
     * @ORM\Column(type="array")
     */
    protected $text = [];

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    protected $link = '';

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $bigEntry = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $active = false;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    protected $position;

    public function __construct()
    {
        $this->text = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Grid
     */
    public function getGrid()
    {
        return $this->grid;
    }

    /**
     * @param Grid $grid
     */
    public function setGrid($grid)
    {
        $this->grid = $grid;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDetailName()
    {
        return $this->detailName;
    }

    /**
     * @param string $detailName
     */
    public function setDetailName($detailName)
    {
        $this->detailName = $detailName;
    }

    /**
     * @return string
     */
    public function getSubHeading()
    {
        return $this->subHeading;
    }

    /**
     * @param string $subHeading
     */
    public function setSubHeading($subHeading)
    {
        $this->subHeading = $subHeading;
    }

    /**
     * @return string
     */
    public function getAccordion()
    {
        return $this->accordion;
    }

    /**
     * @param string $accordion
     */
    public function setAccordion($accordion)
    {
        $this->accordion = $accordion;
    }

    /**
     * @return Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param Media $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return Media
     */
    public function getDetailImage()
    {
        return $this->detailImage;
    }

    public function setDetailImage(Media $detailImage)
    {
        $this->detailImage = $detailImage;
    }

    /**
     * @return array
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param array $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return bool
     */
    public function isBigEntry()
    {
        return $this->bigEntry;
    }

    /**
     * @param bool $bigEntry
     */
    public function setBigEntry($bigEntry)
    {
        $this->bigEntry = $bigEntry;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function fromArrayWithTextManagement(array $array = [], array $fillable = []): self
    {
        // Handle text array.
        $texts = [];
        foreach ($array as $key => $value) {
            if (str_contains($key, 'textField/')) {
                $texts[] = ['text' => $value];
            }
        }
        $array['text'] = $texts;

        return parent::fromArray($array, $fillable);
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'detailName' => $this->getDetailName(),
            'subHeading' => $this->getSubHeading(),
            'accordion' => $this->getAccordion(),
            'image' => $this->getImage(),
            'detailImage' => $this->getDetailImage(),
            'text' => $this->getText(),
            'link' => $this->getLink(),
            'isBigEntry' => $this->isBigEntry(),
            'active' => $this->isActive(),
            'position' => $this->getPosition(),
        ];
    }
}
