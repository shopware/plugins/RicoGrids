<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids\Models;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use RicoGrids\Models\Attributes\Grid as GridAttribute;
use Shopware\Components\Model\ModelEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="RicoGrids\Repositories\GridRepository")
 * @ORM\Table(name="rico_grid")
 */
class Grid extends ModelEntity
{
    public const SORT_POSITION = 1;
    public const SORT_NAME_ASC = 2;
    public const SORT_NAME_DESC = 3;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = null;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $active = false;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Shopware\Models\Shop\Shop")
     * @ORM\JoinTable(name="rico_grid_shop",
     *      joinColumns={
     *          @ORM\JoinColumn(name="grid_id", referencedColumnName="id")
     *      },
     *      inverseJoinColumns={
     *          @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     *      }
     * )
     */
    private $shop = null;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name = '';

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $type = '';

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $subType = '';

    /**
     * @var EntryTemplate
     * @ORM\ManyToOne(targetEntity="RicoGrids\Models\EntryTemplate")
     * @ORM\JoinColumn(name="frontend_layout", referencedColumnName="id", nullable=true)
     */
    private $frontendLayout;

    /**
     * @var GridTemplate
     * @ORM\ManyToOne(targetEntity="RicoGrids\Models\GridTemplate")
     * @ORM\JoinColumn(name="grid_layout", referencedColumnName="id")
     */
    private $gridLayout;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $description = '';

    /**
     * @var ArrayCollection|null
     * @ORM\OneToMany(targetEntity="RicoGrids\Models\GridEntry", mappedBy="grid", cascade={"persist", "remove"})
     */
    private $gridEntries = [];

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $orderType = 1;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $previewSecret = '';

    /**
     * INVERSE SIDE
     *
     * @var GridAttributes
     *
     * @Assert\Valid()
     *
     * @ORM\OneToOne(targetEntity="RicoGrids\Models\Attributes\Grid", mappedBy="grid", cascade={"persist"})
     */
    private $attribute;

    /**
     * Grid constructor.
     */
    public function __construct()
    {
        $this->shop = new ArrayCollection();
        $this->gridEntries = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return ArrayCollection
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param ArrayCollection $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getSubType()
    {
        return $this->subType;
    }

    /**
     * @param string $subType
     */
    public function setSubType($subType)
    {
        $this->subType = $subType;
    }

    /**
     * @return EntryTemplate|null
     */
    public function getFrontendLayout()
    {
        return $this->frontendLayout;
    }

    /**
     * @param EntryTemplate $frontendLayout
     */
    public function setFrontendLayout($frontendLayout)
    {
        $this->frontendLayout = $frontendLayout;
    }

    /**
     * @return GridTemplate|null
     */
    public function getGridLayout()
    {
        return $this->gridLayout;
    }

    /**
     * @param GridTemplate $gridLayout
     */
    public function setGridLayout($gridLayout)
    {
        $this->gridLayout = $gridLayout;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return ArrayCollection|null
     */
    public function getGridEntries()
    {
        return $this->gridEntries;
    }

    /**
     * @param ArrayCollection|null $gridEntries
     */
    public function setGridEntries($gridEntries)
    {
        $this->gridEntries = $gridEntries;
    }

    /**
     * @return string
     */
    public function getPreviewSecret()
    {
        return $this->previewSecret;
    }

    /**
     * @param string $previewSecret
     */
    public function setPreviewSecret($previewSecret)
    {
        $this->previewSecret = $previewSecret;
    }

    /**
     * @return int
     */
    public function getOrderType()
    {
        return $this->orderType;
    }

    /**
     * @param int $orderType
     */
    public function setOrderType($orderType)
    {
        $this->orderType = $orderType;
    }

    /**
     * @return GridAttribute
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * @param GridAttribute|array|null $attribute
     *
     * @return Grid
     */
    public function setAttribute($attribute)
    {
        $this->setOneToOne($attribute, GridAttribute::class, 'attribute', 'grid');

        return $this;
    }

    public function sortEntries(): void
    {
        if (self::SORT_POSITION === $this->getOrderType()) {
            $entries = $this->getGridEntries()->toArray();

            usort($entries, function ($a, $b) {
                /* @var GridEntry $a */
                /* @var GridEntry $b */
                return $a->getPosition() > $b->getPosition() ? 1 : -1;
            });

            $this->setGridEntries(new ArrayCollection($entries));

            return;
        }
        $entries = $this->getGridEntries()->toArray();
        usort($entries, function ($a, $b) {
            /* @var GridEntry $a */
            /* @var GridEntry $b */
            return strcmp(strtolower($a->getName()), strtolower($b->getName()));
        });
        if (self::SORT_NAME_DESC === $this->getOrderType()) {
            $entries = array_reverse($entries);
        }
        $this->setGridEntries(new ArrayCollection($entries));
    }

    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'active' => $this->isActive(),
            'name' => $this->getName(),
            'type' => $this->getType(),
            'subType' => $this->getSubType(),
            'frontendLayout' => $this->getFrontendLayout()->toArray(),
            'gridLayout' => $this->getGridLayout()->toArray(),
            'description' => $this->getDescription(),
            'orderType' => $this->getOrderType(),
            'previewSecret' => $this->getPreviewSecret(),
        ];
    }
}
