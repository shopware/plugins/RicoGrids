<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids\Components\Manager;

use RicoGrids\Components\Translation\TranslationOverlayComponent;
use RicoGrids\Models\Grid;
use RicoGrids\Models\GridEntry;
use RicoGrids\Repositories\GridRepository;
use Shopware\Components\Model\ModelManager;
use Shopware\Components\Plugin\CachedConfigReader;
use Shopware\Models\Shop\Shop;
use Shopware_Components_Modules;
use sRewriteTable;

class SeoUrlManager implements SeoUrlManagerInterface
{
    protected const ORIGINAL_PATH = 'sViewport=RicoGrid';

    /**
     * @var Shopware_Components_Modules
     */
    protected $modules;

    /**
     * @var ModelManager
     */
    protected $modelManager;

    /**
     * @var CachedConfigReader
     */
    protected $configReader;

    /**
     * @var string
     */
    protected $pluginName = '';

    /**
     * @var sRewriteTable
     */
    protected $rewriteTable;

    /**
     * @var GridRepository
     */
    protected $gridRepository;

    /**
     * @var TranslationOverlayComponent
     */
    protected $translationOverlayComponent;

    public function __construct(
        Shopware_Components_Modules $modules,
        ModelManager $modelManager,
        CachedConfigReader $configReader,
        TranslationOverlayComponent $translationOverlayComponent,
        string $pluginName
    ) {
        $this->modules = $modules;
        $this->modelManager = $modelManager;
        $this->configReader = $configReader;
        $this->translationOverlayComponent = $translationOverlayComponent;
        $this->pluginName = $pluginName;
        $this->rewriteTable = $this->modules->RewriteTable();
        $this->gridRepository = $this->modelManager->getRepository(Grid::class);
    }

    public function refreshSeoUrls(Shop $shop): void
    {
        $this->flushExistingUrls($shop->getId());
        $this->buildUrlsForShop($shop);
    }

    protected function flushExistingUrls(int $shopId): void
    {
        $queryBuilder = $this->modelManager->getConnection()->createQueryBuilder();
        $queryBuilder->delete('s_core_rewrite_urls')
            ->where(
                $queryBuilder->expr()->andX(
                    $queryBuilder->expr()->eq('subshopID', ':shopId'),
                    $queryBuilder->expr()->like('org_path', ':path')
                )
            )
            ->setParameter('shopId', $shopId, \PDO::PARAM_INT)
            ->setParameter('path', '%' . self::ORIGINAL_PATH . '%', \PDO::PARAM_STR)
            ->execute();
    }

    protected function buildUrlsForShop(Shop $shop): void
    {
        $grids = $this->gridRepository->findAll();
        if ($shop->getId() > 1) {
            $grids = $this->translationOverlayComponent->translate($shop->getId(), $grids, 'grid');
        }
        /** @var Grid $grid */
        foreach ($grids as $grid) {
            $this->buildGridUrl($shop, $grid);
            $gridEntries = $grid->getGridEntries();
            if ($shop->getId() > 1) {
                $gridEntries = $this->translationOverlayComponent->translate(
                    $shop->getId(),
                    $gridEntries->toArray(),
                    'grid_entry'
                );
            }
            foreach ($gridEntries as $gridEntry) {
                $this->buildGridEntryUrl($shop, $grid, $gridEntry, $grid->getName());
            }
        }
    }

    protected function buildGridUrl(Shop $shop, Grid $grid): void
    {
        $this->insertUrl(
            $shop,
            self::ORIGINAL_PATH . '&action=grid&grid=' . $grid->getId(),
            $grid->getName()
        );
    }

    protected function buildGridEntryUrl(Shop $shop, Grid $grid, GridEntry $gridEntry, string $basePath): void
    {
        $this->insertUrl(
            $shop,
            self::ORIGINAL_PATH . '&action=gridEntry&grid=' . $grid->getId() . '&gridEntry=' . $gridEntry->getId(),
            $basePath . '/' . $gridEntry->getName()
        );
    }

    protected function insertUrl(Shop $shop, string $originalPath, string $seoUrl): void
    {
        $shop->registerResources();
        $this->rewriteTable->sInsertUrl($originalPath, strtolower($this->rewriteTable->sCleanupPath($seoUrl)));
    }
}
