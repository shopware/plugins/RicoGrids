<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids\Loader;

use Doctrine\ORM\EntityManager;
use RicoCustomNavigation\Services\NavigationTypeService;

/**
 * Class RicoMenuConnector
 */
class RicoMenuLoader
{
    /**
     * Defines how the grid-entries are build and accessed
     */
    const GRID_TYPE = [
        'typeArgs' => [
            'filterBy' => [
                'active' => true,
            ],
            'model' => 'RicoGrids\\Models\\Grid',
            'displayField' => 'name',
            'typeName' => 'Grid',
            'parameter' => [
                'grid' => 'id',
            ],
        ],
        'dataArgs' => [
            'type' => 'internal',
            'linkTarget' => '',
            'niceUrl' => true,
            'controllerName' => 'RicoGrid',
            'actionName' => 'grid',
            'parameter' => [],
        ],
    ];

    /**
     * @var EntityManager
     */
    private $manager;

    /**
     * @var NavigationTypeService
     */
    private $typeService;

    /**
     * RicoMenuConnector constructor.
     */
    public function __construct(EntityManager $manager, NavigationTypeService $typeService)
    {
        $this->manager = $manager;
        $this->typeService = $typeService;
    }

    public function registerEntry()
    {
        $this->typeService->registerType(self::GRID_TYPE['typeArgs'], self::GRID_TYPE['dataArgs']);
    }

    public function deRegisterEntry()
    {
        $this->typeService->deRegisterType(self::GRID_TYPE['typeArgs']['typeName']);
    }
}
