<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids\Loader;

use RicoGrids\Factory\EntryTemplateFactory;
use RicoGrids\Factory\GridTemplateFactory;
use Shopware\Components\Model\ModelManager;
use Symfony\Component\Finder\Finder;

/**
 * Class TemplateRegistrationService.
 */
class TemplateRegistrationLoader
{
    /**
     * @var ModelManager
     */
    private $modelManager;

    /**
     * @var string
     */
    private $pluginDirectory;

    /**
     * @var string
     */
    private $registerForAll;

    /**
     * @var string
     */
    private $registerFor;

    /**
     * TemplateRegistrationLoader constructor.
     *
     * @param $modelManager
     * @param string $pluginDirectory
     * @param bool   $registerForAll
     * @param array  $registerFor
     */
    public function __construct($modelManager, $pluginDirectory, $registerForAll = false, $registerFor = [])
    {
        $this->modelManager = $modelManager;
        $this->pluginDirectory = $pluginDirectory;
        $this->registerForAll = $registerForAll;
        $this->registerFor = $registerFor;
    }

    /**
     * @param string $directoryPath
     *
     * @return bool
     */
    public function registerGridTemplates(Finder $symfonyFinder, $directoryPath)
    {
        $templates = [];

        foreach ($symfonyFinder->files()->in($this->pluginDirectory . $directoryPath)->notName('*default*') as $file) {
            array_push($templates, str_replace('.tpl', '', $file->getBasename()));
        }
        array_unique($templates);
        $symfonyFinder->files();

        $this->registerInDatabase($templates, true);

        return true;
    }

    /**
     * @param string $directoryPath
     *
     * @return bool
     */
    public function registerEntryLayoutTemplates(Finder $symfonyFinder, $directoryPath)
    {
        $templates = [];

        foreach ($symfonyFinder->files()->in($this->pluginDirectory . $directoryPath)->notName('*default*') as $file) {
            array_push($templates, str_replace('.tpl', '', $file->getBasename()));
        }
        array_unique($templates);
        $symfonyFinder->files();

        $this->registerInDatabase($templates, false);

        return true;
    }

    /**
     * @param array $templates
     * @param bool  $isGrid
     */
    private function registerInDatabase($templates, $isGrid)
    {
        if ($isGrid) {
            foreach ($templates as $template) {
                $savedTemplate = GridTemplateFactory::build($this->modelManager, $template);
                if (!$savedTemplate) {
                    Shopware()->Container()->get('pluginlogger')->error($template . ' couldn\'t be saved');
                }
            }
        } else {
            foreach ($templates as $template) {
                $savedTemplate = EntryTemplateFactory::build($this->modelManager, $template);
                if (!$savedTemplate) {
                    Shopware()->Container()->get('pluginlogger')->error($template . ' couldn\'t be saved');
                }
            }
        }
    }
}
