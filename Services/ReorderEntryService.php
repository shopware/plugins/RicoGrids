<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids\Services;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use RicoGrids\Models\GridEntry;
use RicoGrids\Repositories\GridEntryRepository;
use Shopware\Components\Model\ModelManager;

/**
 * Class ReorderEntryService
 */
class ReorderEntryService
{
    /**
     * @var ModelManager
     */
    private $modelManager;

    /**
     * @var EntityRepository|GridEntryRepository
     */
    private $repository;

    /**
     * ReorderEntryService constructor.
     */
    public function __construct(ModelManager $modelManager)
    {
        $this->modelManager = $modelManager;
        $this->repository = $modelManager->getRepository(GridEntry::class);
    }

    /**
     * @param int $oldPosition
     * @param int $newPosition
     * @param $gridId
     *
     * @return bool
     */
    public function reOrder($oldPosition, $newPosition, $gridId)
    {
        if ($oldPosition < $newPosition) {
            $this->reOrderLowerItems($this->repository->findToHigher($oldPosition, $newPosition, $gridId));
        } elseif ($oldPosition > $newPosition) {
            $this->reOrderHigherItems($this->repository->findToLower($newPosition, $oldPosition, $gridId));
        }
        try {
            $this->modelManager->flush();

            return true;
        } catch (OptimisticLockException $e) {
            return false;
        }
    }

    /**
     * @param ArrayCollection|GridEntry[] $entries
     */
    private function reOrderLowerItems($entries)
    {
        foreach ($entries as $entry) {
            $entry->setPosition($entry->getPosition() - 1);
            $this->modelManager->persist($entry);
        }
    }

    /**
     * @param ArrayCollection|GridEntry[]$entries
     */
    private function reOrderHigherItems($entries)
    {
        /** @var GridEntry $entry */
        foreach ($entries as $entry) {
            $entry->setPosition($entry->getPosition() + 1);
            $this->modelManager->persist($entry);
        }
    }
}
