<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids\Services;

/**
 * Class CustomTablesService
 */
class CustomTablesService
{
    /**
     * Map of custom attributes
     */
    const custom = [
        'rico_grid_attributes' => [
            'readOnly' => false,
            'model' => 'RicoGrids\Models\Attribute\Grid',
            'identifiers' => ['id', 'grid_id'],
            'foreignKey' => 'grid_id',
            'coreAttributes' => [],
        ],
        'rico_grid_entry_attributes' => [
            'readOnly' => false,
            'model' => 'RicoGrids\Models\Attribute\GridEntry',
            'identifiers' => ['id', 'grid_entry_id'],
            'foreignKey' => 'grid_entry_id',
            'coreAttributes' => [],
        ],
    ];

    /**
     * @param $currentTable
     *
     * @return mixed
     */
    public function addCustomTables($currentTable)
    {
        foreach (self::custom as $tableName => $tableValue) {
            $currentTable[$tableName] = $tableValue;
        }

        return $currentTable;
    }
}
