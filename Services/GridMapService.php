<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids\Services;

use Doctrine\Common\Collections\ArrayCollection;
use RicoGrids\Components\Translation\TranslationOverlayComponent;
use RicoGrids\Models\Grid;
use RicoGrids\Models\GridEntry;
use Shopware\Models\Media\Media;
use Shopware\Models\Shop\Shop;

/**
 * Class GridMapService
 */
class GridMapService
{
    /**
     * @var int
     */
    private $colMax = 4;

    /**
     * @var int
     */
    private $rowMax = 1;

    /**
     * @var array
     */
    private $blocked;

    /**
     * @var Shop
     */
    private $shop;

    /**
     * @var TranslationOverlayComponent
     */
    private $translation;

    /**
     * @var MediaModelFrontendServiceInterface
     */
    private $mediaModelFrontendService;

    public function __construct(
        TranslationOverlayComponent $translation,
        MediaModelFrontendServiceInterface $mediaModelFrontendService
    ) {
        $this->translation = $translation;
        $this->mediaModelFrontendService = $mediaModelFrontendService;
    }

    /**
     *  Will create a perfect mapped frontend Output
     *
     * @param Grid|object $grid
     *
     * @return array
     */
    public function createMap(Grid $grid, Shop $shop)
    {
        $grid->sortEntries();
        $this->shop = $shop;
        $this->colMax = (int) explode('_', $grid->getGridLayout()->getName())[0];
        if (!is_int($this->colMax)) {
            $this->colMax = 4;
        }

        if ('accordion' === $grid->getType()) {
            $gridMap = $this->buildByAccordion($grid);
        } else {
            $gridMap = $this->buildByDefault($grid->getGridEntries());
        }

        // Handle shops in other languages.
        if ($this->shop->getId() > 1) {
            $grid = $this->translation->translate($this->shop->getId(), [$gridMap['grid']], 'grid')[0] ?? null;
            if (!is_null($grid)) {
                $gridEntries = $gridMap['grid']->getGridEntries()->toArray();
                $gridEntries = $this->translation->translate($this->shop->getId(), $gridEntries, 'grid_entry');
            }
            $gridMap['grid'] = $grid;
            $gridMap['grid']->setGridEntries($gridEntries ?? []);
        }

        return 'accordion' === $grid->getType() ? $this->convertAccordionMapToArrayRepresentation($gridMap) :
            $this->convertDefaultMapToArrayRepresentation($gridMap);
    }

    /**
     * @return array
     */
    private function buildByAccordion(Grid $grid)
    {
        $map = [];
        $accordions = [];
        /** @var GridEntry $gridEntry */
        foreach ($grid->getGridEntries() as $gridEntry) {
            if (!$gridEntry->isActive() && '' != $gridEntry->getAccordion() && null !== $gridEntry->getAccordion()) {
                continue;
            }
            if ($accordions[$gridEntry->getAccordion()]) {
                $accordions[$gridEntry->getAccordion()]->add($gridEntry);
            } else {
                $accordions[$gridEntry->getAccordion()] = new ArrayCollection();
                $accordions[$gridEntry->getAccordion()]->add($gridEntry);
                $grid = $gridEntry->getGrid();
            }
        }
        /** @var ArrayCollection|GridEntry[] $accordion */
        foreach ($accordions as $name => $accordion) {
            $map[$name] = $this->buildByDefault($accordion);
            unset($map[$name]['grid']);
            $this->blocked = [];
        }

        return [
            'grid' => $grid,
            'entryMap' => $map,
            'rowAmount' => $this->rowMax,
            'colAmount' => $this->colMax,
        ];
    }

    /**
     * @param GridEntry[]|ArrayCollection $grids
     *
     * @return array
     */
    private function buildByDefault($grids)
    {
        $arrayMap = [];
        $positionX = 1;
        $positionY = 1;
        $grid = null;
        /** @var GridEntry $gridEntry */
        foreach ($grids as $gridEntry) {
            if (!$gridEntry->isActive()) {
                continue;
            }
            $grid = $gridEntry->getGrid();
            $positionYX = $this->getUnblocked($positionY, $positionX, $gridEntry->isBigEntry());
            $coordinates = $this->blockAndCoordinates($gridEntry->isBigEntry(), $positionYX['y'], $positionYX['x']);

            array_push($arrayMap, [
                'gridEntry' => $gridEntry,
                'coordinates' => $coordinates['coordinates'],
            ]);

            if ($gridEntry->isBigEntry()) {
                ++$positionY;
            }
            ++$positionY;
            if ($coordinates['bigToNext']) {
                $positionY = $this->colMax;
            }
            if ($positionY >= $this->colMax + 1) {
                $positionY = 1;
                ++$positionX;
            }
        }

        return [
            'grid' => $grid,
            'entryMap' => $arrayMap,
            'rowAmount' => $this->rowMax,
            'colAmount' => $this->colMax,
        ];
    }

    private function convertDefaultMapToArrayRepresentation(array $gridMap): array
    {
        $gridMap['grid'] = $gridMap['grid']->toArray();
        array_walk($gridMap['entryMap'], function (&$gridItem) {
            $gridItem['gridEntry'] = $gridItem['gridEntry']->toArray();
            $gridItem['gridEntry']['image'] = $gridItem['gridEntry']['image'] instanceof Media ?
                $this->mediaModelFrontendService->convert($gridItem['gridEntry']['image']->getId()) : null;
            $gridItem['gridEntry']['detailImage'] = $gridItem['gridEntry']['detailImage'] instanceof Media ?
                $this->mediaModelFrontendService->convert($gridItem['gridEntry']['detailImage']->getId()) : null;
        });

        return $gridMap;
    }

    private function convertAccordionMapToArrayRepresentation(array $gridMap): array
    {
        $gridMap['grid'] = $gridMap['grid']->toArray();
        foreach ($gridMap['entryMap'] as $tab => $gridItem) {
            array_walk($gridMap['entryMap'][$tab]['entryMap'], function (&$gridItem) {
                $gridItem['gridEntry'] = $gridItem['gridEntry']->toArray();
                $gridItem['gridEntry']['image'] = $gridItem['gridEntry']['image'] instanceof Media ?
                    $this->mediaModelFrontendService->convert($gridItem['gridEntry']['image']->getId()) : null;
                $gridItem['gridEntry']['detailImage'] = $gridItem['gridEntry']['detailImage'] instanceof Media ?
                    $this->mediaModelFrontendService->convert($gridItem['gridEntry']['detailImage']->getId()) : null;
            });
        }

        return $gridMap;
    }

    /**
     * @param $isBig
     * @param $positionY
     * @param $positionX
     *
     * @return array
     */
    private function blockAndCoordinates($isBig, $positionY, $positionX)
    {
        $bigToNexRow = false;
        if ($isBig) {
            if ($positionY === $this->colMax) {
                $bigToNexRow = true;
                $positionY = 1;
                ++$positionX;
            }
            $this->blocked[$positionY][$positionX] = true;
            $this->blocked[$positionY + 1][$positionX] = true;
            $this->blocked[$positionY][$positionX + 1] = true;
            $this->blocked[$positionY + 1][$positionX + 1] = true;
            $coordinates = [
                'y' => [
                    'start' => $positionY,
                    'end' => $positionY + 2,
                ],
                'x' => [
                    'start' => $positionX,
                    'end' => $positionX + 2,
                ],
            ];
        } else {
            $this->blocked[$positionY][$positionX] = true;
            $coordinates = [
                'y' => [
                    'start' => $positionY,
                    'end' => $positionY,
                ],
                'x' => [
                    'start' => $positionX,
                    'end' => $positionX,
                ],
            ];
        }

        return [
            'coordinates' => $coordinates,
            'bigToNext' => $bigToNexRow,
        ];
    }

    /**
     * @param      $positionY
     * @param      $positionX
     * @param bool $isBig
     *
     * @return array
     */
    private function getUnblocked($positionY, $positionX, $isBig = false)
    {
        while (true === $this->blocked[$positionY][$positionX]) {
            ++$positionY;
            if ($positionY >= $this->colMax + 1) {
                $positionY = 1;
                ++$positionX;
                $this->rowMax = $positionX + 1;
            }
            if ($isBig && $positionY === $this->colMax) {
                $positionY = 1;
                ++$positionX;
            }
        }

        return [
            'y' => $positionY,
            'x' => $positionX,
        ];
    }
}
