<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

use Shopware\Models\Media\Media;

class Shopware_Controllers_Frontend_RicoGrid extends Enlight_Controller_Action
{
    /**
     * @var \Shopware\Components\Model\ModelManager
     */
    protected $modelManager;

    public function preDispatch(): void
    {
        $pluginPath = $this->container->getParameter('rico_grids.plugin_dir');
        $this->get('template')->addTemplateDir($pluginPath . '/Resources/views/');
        $this->get('snippets')->addConfigDir($pluginPath . '/Resources/snippets/');
    }

    public function indexAction(): void
    {
        $this->redirect('/404');
    }

    public function gridAction(): void
    {
        $gridId = intval($this->Request()->getParam('grid'));
        $grid = $this->container->get('models')->getRepository(\RicoGrids\Models\Grid::class)->find($gridId);
        $shop = $this->getShop();

        if (!$grid instanceof \RicoGrids\Models\Grid) {
            $this->forward('pageNotFoundError', 'error', 'frontend');

            return;
        }
        $assign = $this->getGridMapService()->createMap($grid, $shop);
        $this->view->assign($assign);
        $this->view->assign(
            'gridAttributes',
            $this->getAttributeGetterService()->get('rico_grid_attributes', 'grid_id', $gridId)
        );
    }

    public function gridEntryAction(): void
    {
        $gridId = (int) $this->Request()->getParam('grid');
        $gridEntryId = (int) $this->Request()->getParam('gridEntry');
        $shop = $this->getShop();
        $mediaService = $this->getMediaModelFrontendService();

        // Validate request arguments.
        if (0 === $gridId || 0 === $gridEntryId) {
            $this->forward('index', 'error', 'frontend');
        }

        // Fetch grid and grid entry.
        /** @var \RicoGrids\Models\Grid $grid */
        $grid = $this->getGridRepository()->find($gridId);
        /** @var \RicoGrids\Models\GridEntry $gridEntry */
        $gridEntry = $this->getGridEntryRepository()->find($gridEntryId);
        $translationComponent = $this->getTranslationOverlayComponent();
        if ($shop->getId() > 1) {
            // Translate grid and grid entry.
            $grid = $translationComponent->translate($shop->getId(), [$grid], 'grid')[0] ?? null;
            $gridEntry = $translationComponent->translate($shop->getId(), [$gridEntry], 'grid_entry')[0] ?? null;
        }

        // Prepare $gridEntry for frontend output.
        $gridEntry = $gridEntry->toArray();
        $gridEntry['image'] = $gridEntry['image'] instanceof Media ?
            $mediaService->convert($gridEntry['image']->getId()) : null;
        $gridEntry['detailImage'] = $gridEntry['detailImage'] instanceof Media ?
            $mediaService->convert($gridEntry['detailImage']->getId()) : null;

        $this->view->assign([
            'grid' => $grid->toArray(),
            'gridEntry' => $gridEntry,
            'gridAttributes' => $this->getAttributeGetterService()->get(
                'rico_grid_attributes',
                'grid_id',
                $gridId
            ),
            'gridEntryAttributes' => $this->getAttributeGetterService()->get(
                'rico_grid_entry_attributes',
                'grid_entry_id',
                $gridEntryId
            ),
        ]);
    }

    protected function getShop(): Shopware\Models\Shop\Shop
    {
        /** @var \Shopware\Models\Shop\Shop $shop */
        $shop = $this->container->get('shop');

        return $shop;
    }

    protected function getGridRepository(): RicoGrids\Repositories\GridRepository
    {
        /** @var \RicoGrids\Repositories\GridRepository $repository */
        $repository = $this->getModelManager()->getRepository(\RicoGrids\Models\Grid::class);

        return $repository;
    }

    protected function getGridEntryRepository(): RicoGrids\Repositories\GridEntryRepository
    {
        /** @var \RicoGrids\Repositories\GridEntryRepository $repository */
        $repository = $this->getModelManager()->getRepository(\RicoGrids\Models\GridEntry::class);

        return $repository;
    }

    protected function getGridMapService(): RicoGrids\Services\GridMapService
    {
        /** @var \RicoGrids\Services\GridMapService $service */
        $service = $this->container->get('rico_grids.service.grid_map');

        return $service;
    }

    protected function getAttributeGetterService(): RicoGrids\Services\AttributeGetterService
    {
        /** @var \RicoGrids\Services\AttributeGetterService $service */
        $service = $this->container->get('rico_grids.services.attribute_getter_service');

        return $service;
    }

    protected function getTranslationOverlayComponent(): RicoGrids\Components\Translation\TranslationOverlayComponent
    {
        /** @var \RicoGrids\Components\Translation\TranslationOverlayComponent $component */
        $component = $this->container->get('rico_grids.service.components.translation.translation_overlay_component');

        return $component;
    }

    protected function getMediaModelFrontendService(): RicoGrids\Services\MediaModelFrontendServiceInterface
    {
        /** @var \RicoGrids\Services\MediaModelFrontendServiceInterface $service */
        $service = $this->container->get('rico_grids.service.media_model_frontend_service');

        return $service;
    }
}
