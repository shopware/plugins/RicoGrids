<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

use Doctrine\Common\Persistence\Proxy;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\Mapping\MappingException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use RicoGrids\Models\Grid;
use RicoGrids\Models\GridEntry;
use Shopware\Components\Model\QueryBuilder;

/**
 * Class Shopware_Controllers_Backend_RicoGrid.
 */
class Shopware_Controllers_Backend_RicoGridEntry extends Shopware_Controllers_Backend_Application
{
    /**
     * @var string
     */
    protected $alias = 'gridEntry';
    /**
     * @var string
     */
    protected $model = GridEntry::class;

    /**
     * @param array $data
     *
     * @throws SmartyException
     * @throws DBALException
     *
     * @return array|GridEntry
     */
    public function save($data)
    {
        $oldPosition = false;
        if ($data['id']) {
            $oldPosition = $this->container->get('models')->find(GridEntry::class, $data['id'])->getPosition();
        }
        $saveData = parent::save($data);
        $gridEntry = $this->container->get('models')->find(GridEntry::class, $saveData['data']['id']);
        if (!$gridEntry->getGrid()) {
            $this->container->get('pluginlogger')->error('Grid couldn\'t be set for the current grid entry.');

            return [
                'success' => 'false',
                'data' => 'Grid couldn\'t be set for the current grid entry.',
            ];
        }
        if ($oldPosition) {
            $this->container->get('rico_grids.services.reoder')
                ->reOrder($oldPosition, $gridEntry->getPosition(), $gridEntry->getId());
        }

        return $saveData;
    }

    /**
     * @return QueryBuilder
     */
    protected function getListQuery()
    {
        $builder = parent::getListQuery();
        $builder->select([$this->alias, 'image', 'detailImage', 'grid'])
            ->leftJoin($this->alias . '.image', 'image')
            ->leftJoin($this->alias . '.detailImage', 'detailImage')
            ->leftJoin($this->alias . '.grid', 'grid');
        if ($this->request->has('grid')) {
            /** @var Grid $grid */
            $grid = $this->container->get('models')->getRepository(Grid::class)->find($this->request->getParam('grid'));
            switch ($grid->getOrderType()) {
                case 1:
                    $builder->orderBy($this->alias . '.name', 'ASC');
                    break;
                case 2:
                    $builder->orderBy($this->alias . '.name', 'DESC');
                    break;
                case 3:
                    $builder->orderBy($this->alias . '.position');
                    break;
            }
            $builder->where($this->alias . '.grid = :grid')
                ->setParameter('grid', $grid);
        }

        return $builder;
    }

    /**
     * @param int $id
     *
     * @return \Doctrine\ORM\QueryBuilder|QueryBuilder
     */
    protected function getDetailQuery($id)
    {
        $builder = $this->getManager()->createQueryBuilder();
        $builder->select([$this->alias, 'image', 'detailImage', 'grid'])
            ->from($this->model, $this->alias)
            ->leftJoin($this->alias . '.image', 'image')
            ->leftJoin($this->alias . '.detailImage', 'detailImage')
            ->leftJoin($this->alias . '.grid', 'grid')
            ->where($this->alias . '.id = :id')
            ->setParameter('id', $id);

        return $builder;
    }

    /**
     * @param array $data
     *
     * @throws MappingException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     *
     * @return array|mixed
     */
    protected function resolveExtJsData($data)
    {
        $metaData = $this->getManager()->getClassMetadata($this->model);
        foreach ($metaData->getAssociationMappings() as $mapping) {
            if (1 === $mapping['type']) {
                $mappingData = $data[$mapping['fieldName']];
                if (array_key_exists(0, $mappingData)) {
                    $data[$mapping['fieldName']] = $data[$mapping['fieldName']][0];
                }
            }

            if (!$mapping['isOwningSide']) {
                continue;
            }

            if (2 === $mapping['type']) {
                /**
                 * @ORM\ManyToOne associations.
                 *
                 * The many to one associations requires that the associated model
                 * will be set in the data array.
                 * To resolve the data we have to find out which column are used for
                 * the mapping. This column is defined in the joinColumns array.
                 * To get the passed id, we need to find out the property name of the column.
                 * After getting the property name of the join column,
                 * we can use the entity manager to find the target entity.
                 */
                $column = $mapping['joinColumns'][0]['name'];
                $field = $metaData->getFieldForColumn($column);
                if ($data[$field]) {
                    $associationModel = $this->getManager()->find($mapping['targetEntity'], $data[$field]);

                    //proxies need to be loaded, otherwise the validation will be failed.
                    if ($associationModel instanceof Proxy && method_exists($associationModel, '__load')) {
                        $associationModel->__load();
                    }
                    $data[$mapping['fieldName']] = $associationModel;
//                    unset($data[$field]);
                }
            } elseif (3 === $mapping['type']) {
                $associationData = $data[$mapping['fieldName']];
                $associationModels = [];
                foreach ($associationData as $singleData) {
                    $associationModel = $this->getManager()->find($mapping['targetEntity'], $singleData['id']);
                    if ($associationModel) {
                        $associationModels[] = $associationModel;
                    }
                }
                $data[$mapping['fieldName']] = $associationModels;
            }
        }

        return $data;
    }

    /**
     * @return Shopware_Components_Translation
     */
    protected function getTranslationComponent()
    {
        if (null === $this->translation) {
            $this->translation = $this->container->get('translation');
        }

        return $this->translation;
    }
}
