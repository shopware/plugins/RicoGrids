<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

use Doctrine\ORM\Mapping\ClassMetadataInfo;
use RicoGrids\Models\Grid;
use Shopware\Components\Model\QueryBuilder;
use Shopware\Models\Shop\Shop;

/**
 * Class Shopware_Controllers_Backend_RicoGrid
 */
class Shopware_Controllers_Backend_RicoGrids extends Shopware_Controllers_Backend_Application
{
    /**
     * @var string
     */
    protected $alias = 'grid';
    /**
     * @var string
     */
    protected $model = Grid::class;

    /**
     * @var Shopware_Components_Translation
     */
    private $translation;

    /**
     * Used for the url generation by ClearSEOUrls via cache
     */
    public function seoRicoGridAction()
    {
        @set_time_limit(1200);
        $shopId = (int) $this->Request()->getParam('shopId');
        /** @var Shopware_Components_SeoIndex $seoIndex */
        $seoIndex = $this->container->get('SeoIndex');
        $shop = $seoIndex->registerShop($shopId);
        /** @var Shopware_Components_Modules $modules */
        $modules = $this->container->get('modules');
        $rewriteTable = $modules->RewriteTable();
        $rewriteTable->baseSetup();
        /** @var \RicoGrids\Components\Manager\SeoUrlManagerInterface $seoUrlManager */
        $seoUrlManager = $this->container->get('rico_grids.service.components.manager.seo_url_manager');
        $seoUrlManager->refreshSeoUrls($shop);
        $this->View()->assign([
            'success' => true,
        ]);
    }

    /**
     * @throws SmartyException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function createAction()
    {
        $save = parent::save($this->request->getParams());

        if (!$save['success']) {
            $this->View()->assign(
                $save
            );
        }
        $grid = $this->container->get('models')
            ->find(Grid::class, $save['data']['id']);
        $this->View()->assign(
            $save
        );
    }

    /**
     * @throws SmartyException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function updateAction()
    {
        $oldGridName = $this->container->get('models')->getRepository(Grid::class)
            ->find(
                $this->request->getParam('id')
            )->getName();
        $save = parent::save($this->request->getParams());
        if (!$save['success']) {
            $this->View()->assign(
                $save
            );
        }
        $grid = $this->container->get('models')
            ->find(Grid::class, $save['data']['id']);
        $this->View()->assign(
            $save
        );
    }

    /**
     * @param int $id
     *
     * @return \Doctrine\ORM\QueryBuilder|QueryBuilder
     */
    protected function getDetailQuery($id)
    {
        $builder = parent::getDetailQuery($id);
        $builder->select([$this->alias, 'shop', 'gridLayout', 'frontendLayout'])
            ->leftJoin($this->alias . '.shop', 'shop')
            ->leftJoin($this->alias . '.gridLayout', 'gridLayout')
            ->leftJoin($this->alias . '.frontendLayout', 'frontendLayout');

        return $builder;
    }

    /**
     * Helper function which resolves the passed Ext JS data of an model.
     * This function resolves the following associations automatically:
     *
     * @ORM\OneToOne() associations
     *      => Ext JS sends even for @ORM\OneToOne() associations, a multi dimensional array
     *      => array('billing' => array( 0 => array('id' => ...) ))
     *      => The function removes the first level of the array to have to model data directly in the association property.
     *      => array('billing' => array('id' => ...))
     *
     * @ORM\ManyToOne() associations
     *      => @ORM\ManyToOne() requires the related doctrine model in the association key property.
     *      => But Ext JS sends only the foreign key property.
     *      => 'article' => array('id' => 1, ... , 'shopId' => 1, 'shop' => null)
     *      => This function resolves the foreign key, removes the foreign key property from the data array and sets the founded doctrine model into the association property.
     *      => 'article' => array('id' => 1, ... , 'shop' => $this->getManager()->find(Model, $data['shopId']);
     *
     * @ORM\ManyToMany() associations
     *      => @ORM\ManyToMany() requires like the @ORM\ManyToOne() associations the resolved doctrine models in the association property.
     *      => But Ext JS sends only an array of foreign keys.
     *      => 'article' => array('id' => 1, 'categories' => array(array('id'=>1), array('id'=>2), ...)
     *      => This function iterates the association property and resolves each foreign key value with the corresponding doctrine model
     *      => 'article' => array('id' => 1, 'categories' => array($this->getManager()->find(Model, 1), $this->getManager()->find(Model, 2), ...)
     *
     * @param array $data
     */
    protected function resolveExtJsData($data)
    {
        $metaData = $this->getManager()->getClassMetadata($this->model);

        foreach ($metaData->getAssociationMappings() as $mapping) {
            /*
             * @ORM\OneToOne associations
             *
             * Ext JS sends even for one to one associations multi dimensional array with association data.
             * @example:
             *    model:            Shopware\Models\Customer\Customer
             *    association:      $billing  (Shopware\Models\Customer\Billing)
             *    required data:    array(
             *                          'id' => 1,
             *                          'billing' => array(
             *                              'street' => '...',
             *                              ...
             *                          )
             *                      )
             *
             *    Ext JS data:      array(
             *                          'id' => 1,
             *                          'billing' => array(
             *                              0 => array(
             *                                  'street' => '...',
             *                                  ...
             *                              )
             *                          )
             *                      )
             *
             * So we have to remove the first level of the posted data.
             */
            if (ClassMetadataInfo::ONE_TO_ONE === $mapping['type']) {
                $mappingData = $data[$mapping['fieldName']];
                if (array_key_exists(0, (array) $mappingData)) {
                    $data[$mapping['fieldName']] = $data[$mapping['fieldName']][0];
                }
            }

            if (!$mapping['isOwningSide']) {
                continue;
            }

            if (ClassMetadataInfo::MANY_TO_ONE === $mapping['type']) {
                /**
                 * @ORM\ManyToOne associations.
                 *
                 * The many to one associations requires that the associated model
                 * will be set in the data array.
                 * To resolve the data we have to find out which column are used for
                 * the mapping. This column is defined in the joinColumns array.
                 * To get the passed id, we need to find out the property name of the column.
                 * After getting the property name of the join column,
                 * we can use the entity manager to find the target entity.
                 */
                $column = $mapping['joinColumns'][0]['name'];
                $field = $metaData->getFieldForColumn($column);

                if ($data[$field]) {
                    $associationModel = $this->getManager()->find($mapping['targetEntity'], $data[$field]);

                    //proxies need to be loaded, otherwise the validation will be failed.
                    if ($associationModel instanceof \Doctrine\Common\Persistence\Proxy && method_exists($associationModel, '__load')) {
                        $associationModel->__load();
                    }
                    $data[$mapping['fieldName']] = $associationModel;

                    //remove the foreign key data.
//                    unset($data[$field]);
                }
            } elseif (ClassMetadataInfo::MANY_TO_MANY === $mapping['type']) {
                /**
                 * @ORM\ManyToMany associations.
                 *
                 * The data of many to many association are contained in the corresponding field:
                 *
                 * @example
                 *    model:        Shopware\Models\Article\Article
                 *    association:  $categories  (mapping table: s_articles_categories)
                 *    joined:       - s_articles.id <=> s_articles_categories.articleID
                 *                  - s_categories.id <=> s_articles_categories.categoryID
                 *
                 *    passed data:  'categories' => array(
                 *                      array('id'=>1, ...),
                 *                      array('id'=>2, ...),
                 *                      array('id'=>3, ...)
                 *                  )
                 */
                $associationData = $data[$mapping['fieldName']];
                $associationModels = [];
                foreach ($associationData as $singleData) {
                    $associationModel = $this->getManager()->find($mapping['targetEntity'], $singleData['id']);
                    if ($associationModel) {
                        $associationModels[] = $associationModel;
                    }
                }
                $data[$mapping['fieldName']] = $associationModels;
            }
        }

        return $data;
    }

    /**
     * @return Shopware_Components_Translation
     */
    protected function getTranslationComponent()
    {
        if (null === $this->translation) {
            $this->translation = $this->container->get('translation');
        }

        return $this->translation;
    }
}
