<?php

/**
 * MIT License
 *
 * Copyright (c) 2020 PSVneo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

declare(strict_types=1);

namespace RicoGrids;

use Exception;
use RicoGrids\Loader\DatabaseLoader;
use RicoGrids\Loader\RicoMenuLoader;
use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;

/**
 * Class RicoGrids
 */
class RicoGrids extends Plugin
{
    /**
     * @throws Exception
     */
    public function install(InstallContext $context)
    {
        $modelManager = $this->container->get('models');
        $kernelEnv = $this->container->get('kernel')->getEnvironment();
        $pluginManager = $this->container->get('shopware_plugininstaller.plugin_manager');

        $database = new DatabaseLoader(
            $modelManager
        );
        $database->install();

        // region generator

        $this->container->get('models')
            ->generateAttributeModels(['rico_grid_attributes', 'rico_grid_entry_attributes']);

        // endregion

//        region add to navigation
        if ($pluginManager->getPluginByName('RicoCustomNavigation')
            && $pluginManager->getPluginByName('RicoCustomNavigation')->getActive()
            && $this->container->has('rico_custom_navigation.type_service')) {
            $menuRegistration = new RicoMenuLoader(
                $modelManager,
                $this->container->get('rico_custom_navigation.type_service'));
            $menuRegistration->registerEntry();
        }
//        endregion

        parent::install($context);
    }

    /**
     * @throws Exception
     */
    public function uninstall(UninstallContext $context)
    {
        if (!$context->keepUserData()) {
            $database = new DatabaseLoader(
                $this->container->get('models')
            );
            $database->uninstall();

            $pluginManager = $this->container->get('shopware_plugininstaller.plugin_manager');
            if ($pluginManager->getPluginByName('RicoCustomNavigation')
                && $pluginManager->getPluginByName('RicoCustomNavigation')->getActive()
                && $this->container->has('rico_custom_navigation.type_service')) {
                $menuRegistration = new RicoMenuLoader(
                    $this->container->get('models'),
                    $this->container->get('rico_custom_navigation.type_service'));
                $menuRegistration->deRegisterEntry();
            }
        }

        parent::uninstall($context);
    }
}
